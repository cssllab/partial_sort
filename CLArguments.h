#ifndef CL_ARGUMENTS_H
#define CL_ARGUMENTS_H

struct CLArguments{
	int numberOfRows;
	int numberOfColumns;
	int k;
	int devInd;
	unsigned int warpsPB;
	bool extraComparison;
	bool valuesOnly;

	char fStatName[32];
	
	size_t totalSize()const{
		return numberOfRows*rowLength();
	}

	__host__ __device__
	size_t rowLength()const{
		return (numberOfColumns);
	}
};

#endif