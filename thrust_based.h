#ifndef THRUST_BASED_H
#define THRUST_BASED_H

struct CLArguments;

void ThrustSort(float *dist, int *rowIndices, int *columnIndices, CLArguments const &clArgs);
void ThrustSortComp(float *dist, int *rowIndices, int *columnIndices, CLArguments const &clArgs);
void CheckIfSorted(float const *dist, CLArguments const &clArgs, bool checkRowIndices=false, int const *rowIndices=NULL);

#endif