#ifndef DATA_TYPES_H
#define DATA_TYPES_H

struct KeyIndexArray{
	float *values;
	int *indices;

	__inline__ __device__ __host__
		KeyIndexArray(){}

	__inline__ __device__ __host__
	KeyIndexArray(float *v, int *i):values(v), indices(i){}
};

__inline__ __device__
KeyIndexArray shifted(KeyIndexArray arg, int shift){
	arg.values+=shift;
	arg.indices+=shift;

	return arg;
}

__inline__ __device__
float * shifted(float *arg, int shift){
	arg+=shift;
	return arg;
}

__inline__ __device__
void shift(KeyIndexArray &arg, int shift){
	arg.values+=shift;
	arg.indices+=shift;
}

struct KeyArray{
	float *values;

	__inline__ __device__ __host__
	KeyArray(){}

	__inline__ __device__
	KeyArray(float *v):values(v){}
};

__inline__ __device__
KeyArray shifted(KeyArray arg, int shift){
	arg.values+=shift;

	return arg;
}

__inline__ __device__
void shift(KeyArray &arg, int shift){
	arg.values+=shift;
}


__inline__ __device__
void shift(float *&arg, int shift){
	arg+=shift;
}

#endif
