PROJECT(PartialSort)

CMAKE_MINIMUM_REQUIRED(VERSION 2.8.5)

FIND_PACKAGE(CUDA REQUIRED)



set(CMAKE_SUPPRESS_REGENERATION TRUE)

set(CUDA_NVCC_FLAGS -arch=sm_20)


SET (SOURCE_FILES
	kernels.cu
	thrust_based.cu
	check_funcs.cu
	main.cu
)

SET(INCLUDE_FILES
	queue.cuh
	ColorText.h
	MyTime.h
	thrust_based.h
	rng.h
	kernels.h
	CLArguments.h
	check_funcs.h
)

SOURCE_GROUP(SourceFiles FILES
	${SOURCE_FILES})
	
SOURCE_GROUP(includes FILES
	${INCLUDE_FILES})

CUDA_ADD_EXECUTABLE(PartialSort	
	${SOURCE_FILES}
	${INCLUDE_FILES}
)
