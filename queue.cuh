#ifndef QUEUE_H
#define QUEUE_H

template<typename T>
class Queue{

	class Node{
		template<typename intT>
		friend class Queue;

		Node *next;
		T val;

		__device__ inline
		explicit Node(T val_):next(NULL), val(val_){
			//printf("Node accepted value %d\n", val);
		}
	};
	Queue(const Queue &);
	Queue& operator=(const Queue &);

	Node *first;
	Node *last;
public:
	__device__ inline
	Queue():first(NULL), last(NULL){}

	__device__ inline
	void push_back(T val){
		//assert(threadIdx.x==0);
		Node *newNode = new Node(val);
		//printf("new node created with value %d\n", val);
		if(first==NULL){
			first=newNode;
			last=newNode;
			//printf("adding first node\n");
		}
		else{
			last->next=newNode;
			//printf("modyfing tail\n");
			last=newNode;
		}
//		assert(last->next==NULL);
	}

	__device__ inline 
	bool is_empty()const{
		if(first==NULL&&last!=NULL){
			//printf("failed here 1\n");
		}
		if(first!=NULL&&last==NULL){
			//printf("failed here 2\n");
		}
//		assert((first==NULL&&last==NULL)||(first!=NULL&&last!=NULL));
		return first==NULL;
	}

	__device__ inline
	T pop_front(){
//		assert(threadIdx.x==0);
//		assert(!is_empty());
		T res=first->val;

		Node *currNode=first;
		first=currNode->next;

		if(first==NULL){
			//printf("popped last node\n");
			last=NULL;
		}
		
		delete currNode;

		return res;
	}
};

#endif