#pragma warning (disable: 4244)
#include "check_funcs.h"
#include "CLArguments.h"
#include "ColorText.h"

#pragma warning (disable: 4244)

#include <thrust/device_ptr.h>
#include <thrust/extrema.h>
#include <thrust/sort.h>
#include <iostream>
#include <sstream>
#include <exception>

using std::cout;
using std::cerr;
using std::endl;
using std::stringstream;
using thrust::device_vector;
using thrust::device_ptr;

void PrintAtMost(int maxEl, CLArguments clArgs, device_vector<int> const &dist, device_vector<int> const &distSelected);

void CheckSolution(float *dist, CLArguments const &clArgs){
	device_ptr<float> thrustDist=thrust::device_pointer_cast(dist);
	cout<<"Check if solution is correct... ";
	for(int i=0; i<clArgs.numberOfRows; ++i){
		device_ptr<float> thrustDistCurrentRow=thrustDist+clArgs.rowLength()*i;

		/*printf("%e\n", (float)(*(thrustDistCurrentRow+clArgs.k)));
		for(size_t j=0; j<clArgs.rowLength()-1; ++j){
			printf("%d %e\n", j,(float)thrustDistCurrentRow[j]);
		}*/
		
		float maxFromFound;
		float minFromLeft;
		try{
			maxFromFound=*thrust::max_element(thrustDistCurrentRow, thrustDistCurrentRow+clArgs.k);
		
		}catch(...){
			cerr<<"Error (first) in row "<<i<<"; k="<<clArgs.k<<endl;
			throw;
		}
		try{
			minFromLeft=*thrust::min_element(thrustDistCurrentRow+clArgs.k, thrustDistCurrentRow+clArgs.rowLength());

			if(minFromLeft<maxFromFound){
				cerr<<endl<<"\trow "<<i<<" is unsorted; "<<minFromLeft<<" "<<maxFromFound<<endl;
			exit(EXIT_FAILURE);
		}
		}catch(...){
			cerr<<"Error in row "<<i<<"; k="<<clArgs.k<<" part length="<<
				thrust::distance(thrustDistCurrentRow+clArgs.k, thrustDistCurrentRow+clArgs.rowLength())<<
				" row length="<<clArgs.rowLength()<<endl;
			throw;
		}
		
	}
	cout<<"done."<<endl;
}

struct DistComp{

	inline __device__
	bool operator()(thrust::tuple<float, int> lhs, thrust::tuple<float, int> rhs)const{
		/*if(thrust::get<0>(lhs)==thrust::get<0>(rhs))
			return thrust::get<1>(lhs)<thrust::get<1>(rhs);
		else*/
			return thrust::get<0>(lhs)<thrust::get<0>(rhs);
	}
};

void CheckSolution(device_vector<float> &distToCheck, device_vector<int> &columnIndicesToCheck, 
	device_vector<float> const &distRef,//sorted
	device_vector<int> const &columnIndicesRef,//sorted
	CLArguments const &clArgs)
{
	cout<<ColorText(FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_INTENSITY)<<"Checking if solution is correct... ";

	//PrintAtMost(clArgs.k, clArgs, dist, distSelected);

	device_vector<int> indChecker1(clArgs.k), indChecker2(clArgs.k);
	for(int i=0; i<clArgs.numberOfRows; ++i){
		device_vector<float>::iterator rowDist=distToCheck.begin()+clArgs.rowLength()*i;

		/*{
			cerr<<endl;
			for(int i=0; i<clArgs.k; ++i){
				cerr<<rowDist[i]<<" ";
			}
			cerr<<endl;
		}*/

		device_vector<float>::iterator indDist=distToCheck.begin()+clArgs.rowLength()*i;
		int refRowShift=clArgs.k*i;
		thrust::sort_by_key(make_zip_iterator(make_tuple(rowDist, indDist)), 
			make_zip_iterator(make_tuple(rowDist+clArgs.k,indDist+clArgs.k)), indDist, DistComp());

		/*{
			cerr<<endl;
			for(int i=0; i<clArgs.k; ++i){
				cerr<<rowDist[i]<<" ";
			}
			cerr<<endl;
		}*/


		if(!thrust::equal(rowDist, rowDist+clArgs.k, distRef.begin()+refRowShift)){

			thrust::host_vector<float> h_rw(rowDist, rowDist+clArgs.k);
			thrust::host_vector<float> h_ds(distRef.begin()+refRowShift, distRef.begin()+refRowShift+clArgs.k);

			stringstream errStr;
			errStr<<"distances are not equal in row #"<<i<<endl;
			errStr<<"index/extracted/reference"<<endl;

			for(int j=0; j<clArgs.k; ++j){
				errStr<<j<<" "<<h_rw[j]<<" "<<h_ds[j]<<"\n";
			}


			throw std::runtime_error(errStr.str().c_str());
		}

	/*	if(!thrust::equal(indDist, indDist+clArgs.k, columnIndicesSelected.begin()+rowShift)){
			

			stringstream errStr;
			errStr<<"indices are not equal in row #"<<i;
			for(int j=0; j<clArgs.k; ++j){
				int i1=indDist[j];
				int i2=columnIndicesSelected[rowShift+j];
				if(i1!=i2){
					errStr<<"\n\tFirst error in index: "<<j<<" observed: "<<i1<<"/"<<rowDist[j]<<" expected: "<<i2<<"/"<<distSelected[rowShift+j]<<
						"\n\tPrevious are: "<<indDist[j-1]<<"/"<<rowDist[j-1]<<" and "<<columnIndicesSelected[rowShift+j-1]<<"/"<<distSelected[rowShift+j-1]<<
						"\n\tNext are: "<<indDist[j+1]<<"/"<<rowDist[j+1]<<" and "<<columnIndicesSelected[rowShift+j+1]<<"/"<<distSelected[rowShift+j+1]<<
						"\n\tSecond are: "<<indDist[j+2]<<"/"<<rowDist[j+2]<<" and "<<columnIndicesSelected[rowShift+j+2]<<"/"<<distSelected[rowShift+j+2]<<
						"\n\tThird are: "<<indDist[j+3]<<"/"<<rowDist[j+3]<<" and "<<columnIndicesSelected[rowShift+j+3]<<"/"<<distSelected[rowShift+j+3]<<endl;
					break;
				}
			}
			
			throw std::exception(errStr.str().c_str());
		}*/

		/*thrust::copy(indices.begin()+clArgs.rowLength()*i, indices.begin()+clArgs.rowLength()*i+clArgs.k, indChecker1.begin());
		thrust::copy(columnIndicesSelected.begin()+rowShift, columnIndicesSelected.begin()+rowShift+clArgs.k, indChecker2.begin());

		thrust::sort(indChecker1.begin(), indChecker1.end());
		thrust::sort(indChecker2.begin(), indChecker2.end());

		if(!thrust::equal(indChecker1.begin(), indChecker1.end(), indChecker2.begin())){
			

			stringstream errStr;
			errStr<<"indices are not equal in row #"<<i;
			for(int j=0; j<clArgs.k; ++j){
				int i1=indChecker1[j];
				int i2=indChecker2[j];
				if(i1!=i2){
					errStr<<"\n\t first error in index: "<<j<<" expected: "<<i1<<" observed: "<<i2<<endl;
					break;
				}
			}
			
			throw std::exception(errStr.str().c_str());
		}*/
	}

	cout<<ColorText(FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_INTENSITY)<<"done\n";
}
