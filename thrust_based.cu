#include "thrust_based.h"
#include "CLArguments.h"

#pragma warning (disable: 4244)

#include <thrust/device_ptr.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/sort.h>
#include <thrust/logical.h>
#include <iostream>

using std::cout;
using std::cerr;
using std::endl;
using thrust::make_zip_iterator;
using thrust::device_ptr;

struct EqualToInt{
	int m_val;
	EqualToInt(int val):m_val(val){}
	__device__
	bool operator()(int i)const{
		return i==m_val;
	}
};

void ThrustSort(float *dist, int *rowIndices, int *columnIndices, CLArguments const &clArgs){
	device_ptr<float> thrustDist=thrust::device_pointer_cast(dist);
	device_ptr<int> thrustRowIndices=thrust::device_pointer_cast(rowIndices);
	device_ptr<int> thrustColumnIndices=thrust::device_pointer_cast(columnIndices);
	thrust::sort_by_key(thrustDist, thrustDist+clArgs.totalSize(), make_zip_iterator(make_tuple(thrustRowIndices,thrustColumnIndices))); 
	thrust::stable_sort_by_key(thrustRowIndices, thrustRowIndices+clArgs.totalSize(), make_zip_iterator(make_tuple(thrustDist, thrustColumnIndices)));
}

struct RowComp{
	__device__ inline
	bool operator()(thrust::tuple<float, int> const lhs, thrust::tuple<float, int> const rhs)const{
		if(thrust::get<1>(lhs)==thrust::get<1>(rhs))
			return thrust::get<0>(lhs)<thrust::get<0>(rhs);
		else
			return thrust::get<1>(lhs)<thrust::get<1>(rhs);
	}
};

void ThrustSortComp(float *dist, int *rowIndices, int *columnIndices, CLArguments const &clArgs){
	device_ptr<float> thrustDist=thrust::device_pointer_cast(dist);
	device_ptr<int> thrustRowIndices=thrust::device_pointer_cast(rowIndices);
	device_ptr<int> thrustColumnIndices=thrust::device_pointer_cast(columnIndices);

	thrust::sort_by_key(make_zip_iterator(make_tuple(thrustDist, thrustRowIndices)),
		make_zip_iterator(make_tuple(thrustDist+clArgs.totalSize(), thrustRowIndices + clArgs.totalSize())),
		thrustDist, RowComp()); 
	
}

void CheckIfSorted(float const *dist, CLArguments const &clArgs, bool checkRowIndices, int const *rowIndices){
	device_ptr<float const> thrustDist=thrust::device_pointer_cast(dist);
	device_ptr<int const> thrustIndices=thrust::device_pointer_cast(rowIndices);
	cout<<"Check if arrays are sorted... ";
	for(int i=0; i<clArgs.numberOfRows; ++i){
		device_ptr<float const> thrustDistCurrentRow=thrustDist+clArgs.rowLength()*i;
		device_ptr<int const> thrustIndicesCurrentRow=thrustIndices+clArgs.rowLength()*i;
		if(!thrust::is_sorted(thrustDistCurrentRow, thrustDistCurrentRow+clArgs.rowLength())){
			cerr<<endl<<"Distances are not sorted for a row index #"<<i<<endl;
			exit(EXIT_FAILURE);
		}

		if(checkRowIndices&&!thrust::all_of(thrustIndicesCurrentRow, thrustIndicesCurrentRow+clArgs.rowLength(), EqualToInt(i))){
			cerr<<endl<<"Indices row are not sorted for a row index #"<<i<<endl;
			exit(EXIT_FAILURE);
		}
	}
	cout<<"done."<<endl;
}
