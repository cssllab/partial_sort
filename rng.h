#ifndef RNG_H
#define RNG_H

#include <thrust/random.h>
#include <thrust/random/uniform_int_distribution.h>

class RNG{
	//thrust::random::taus88 rng;
	thrust::random::minstd_rand0 rng;
	//thrust::random::ranlux24_base rng;

public:
	__forceinline__
	__device__ 
	int intInRange(int a, int b, int rowIndex)
	{
		rng.discard(rowIndex); 
		thrust::uniform_int_distribution<int> uid(a, b);

		return uid(rng);
	}
};


class RNG1{
	//thrust::random::taus88 rng;
	thrust::random::minstd_rand0 rng;//(hash(seed));
	//thrust::random::ranlux24_base rng;

	__device__
	static unsigned int hash(unsigned int x) {
		x = ((x >> 16) ^ x) * 0x45d9f3b;
		x = ((x >> 16) ^ x) * 0x45d9f3b;
		x = ((x >> 16) ^ x);
		//printf("x=%d\n",x);
		return x;
	}

public:
	__device__ 
	RNG1(unsigned int seed):rng(hash(seed)){}

	__forceinline__
	__device__ 
	int intInRange(int a, int b, int rowIndex)
	{
		rng.discard(rowIndex); 
		thrust::uniform_int_distribution<int> uid(a, b);

		return uid(rng);
	}
};

#endif
