﻿#include "kernels.h"
#include <stdio.h>
#include "rng.h"

#include "DataTypes.h"

#include "queue.cuh"


#include <iostream>
#include <cassert>
#include <cfloat>
#include <stdexcept>


using std::cout;
using std::endl;
using std::cerr;


//needs all 32 threads in a warp to run this function
//val is an input value for the current thread
//takes extra array of 64 elements in a shared memory to perform summation
template <typename T>
__forceinline__
__device__ T inclusive_warp_scan(int idx, T val, volatile T *s_data)
{
	//assert(s_data[idx+32]==(T)0);
	assert(idx<warpSize);
	assert(s_data[idx]==(T)0);
	
//	s_data[idx]=0.;
	idx+=warpSize;
	assert(idx<warpSize*2);
	T t=s_data[idx]=val;
	
	assert(s_data[idx-32]==(T)0);

/*#ifndef NDEBUG
	if(idx==32){
		for(int i=32; i<32+warpSize; ++i){
			if(s_data[i]!=0)
				printf("error in inclusive_warp_scan: must be zeros %d, %d\n", i, s_data[i]);
		}
	}
#endif*/

	s_data[idx]=t=t+s_data[idx-1];
	s_data[idx]=t=t+s_data[idx-2];
	s_data[idx]=t=t+s_data[idx-4];
	s_data[idx]=t=t+s_data[idx-8];
	s_data[idx]=t=t+s_data[idx-16];
	return s_data[idx];
}

template <typename T>
__forceinline__ __device__
void warpCopy(int inWarpIndex, T* dest, T const *source, int length){

	int chunksInRow=DivUp(length,warpSize);

	for(int i=0; i<chunksInRow; ++i){
		int inChunkIndex=inWarpIndex+i*warpSize;
		if(inChunkIndex>=length)
			continue;
		dest[inChunkIndex]=source[inChunkIndex];
	}
}

__forceinline__ __device__
void warpCopy(int inWarpIndex, KeyIndexArray dest, KeyIndexArray source, int length){

	int chunksInRow=DivUp(length,warpSize);

	for(int i=0; i<chunksInRow; ++i){
		int inChunkIndex=inWarpIndex+i*warpSize;
		if(inChunkIndex>=length)
			continue;

		dest.indices[inChunkIndex]=source.indices[inChunkIndex];
		dest.values[inChunkIndex]=source.values[inChunkIndex];
	}
}


__forceinline__ __device__
void blockCopy(int inBlockIndex, float* dest, float const *source, int length){

	assert(inBlockIndex<blockDim.x);

	int chunksInRow=DivUp(length,blockDim.x);

	for(int i=0; i<chunksInRow; ++i){
		int inChunkIndex=inBlockIndex+i*blockDim.x;
		if(inChunkIndex>=length)
			continue;
		dest[inChunkIndex]=source[inChunkIndex];
	}
}


__forceinline__ __device__
void blockCopy(int inBlockIndex, KeyIndexArray dest, KeyIndexArray source, int length){

	assert(inBlockIndex<blockDim.x);

	int chunksInRow=DivUp(length,blockDim.x);

	for(int i=0; i<chunksInRow; ++i){
		int inChunkIndex=inBlockIndex+i*blockDim.x;
		if(inChunkIndex>=length)
			continue;

		dest.indices[inChunkIndex]=source.indices[inChunkIndex];
		dest.values[inChunkIndex]=source.values[inChunkIndex];
	}
}


/*template <typename T>
__forceinline__ __device__
void blockCopy(int inBlockIndex, T dest, T const source, int length){

	int chunksInRow=DivUp(length,blockDim.x);

	for(int i=0; i<chunksInRow; ++i){
		int inChunkIndex=inBlockIndex+i*blockDim.x;
		if(inChunkIndex>=length)
			continue;
		copy(dest, inChunkIndex, source, inChunkIndex);
		//dest[inChunkIndex]=source[inChunkIndex];
	}

}*/

inline __device__
void CheckIfPartitioned(int inWarpIndex, int chunkSize, int destPivotInd, float pivot, int initialPivotInd, float const *destDistArrayBegin){
	if(inWarpIndex==0){
		for(int i=0; i<chunkSize; ++i){
			if(i<=destPivotInd){

				assert(destDistArrayBegin[i]<=pivot);
				/*if(!(destDistArrayBegin[i]<=pivot)){
					printf("%d %e %e\n", i, destDistArrayBegin[i], pivot);
				}*/
			}
			else if(i>destPivotInd){
				//assert(destDistArrayBegin[i]>pivot);
			}
			//else{
			//	assert(destDistArrayBegin[i]==pivot);
			//}
		}
	}
}

__forceinline__ __device__ 
int PartitionInclusiveScan(int rowIndex, int inWarpIndex,
    int warpInBlockInd,
	volatile int *s_pivotInd,
	const float *sourceDistArrayBegin, const float *sourceDistArrayEnd,
	const int *sourceIndicesArrayBegin,	
	float *destDistArrayBegin,
	int *destIndArrayBegin, 
	RNG &rng)
{
	int chunkSize=sourceDistArrayEnd-sourceDistArrayBegin;

	int chunksInRow=DivUp(chunkSize,warpSize);
	
	if(inWarpIndex==0){
		*s_pivotInd=rng.intInRange(0, chunkSize-1, rowIndex);

		//if(*s_pivotInd==0)
			//printf("next random: %d %d %d\n", PivotInd(0, chunkSize-1, rowIndex, rng), chunkSize-1, rowIndex);
	}

	//__syncthreads();

	__shared__ int s_L1Common[32*2*warpsPerBlock], s_L2Common[32*2*warpsPerBlock];

	int *s_L1=s_L1Common+warpInBlockInd*64;
	int *s_L2=s_L2Common+warpInBlockInd*64;

	assert(*s_pivotInd>=0&&*s_pivotInd<chunkSize);
		
	s_L1[inWarpIndex]=0;
	s_L2[inWarpIndex]=0;

	float pivot=sourceDistArrayBegin[*s_pivotInd];
	for(int i=0; i<chunksInRow; ++i){
		int inChunkIndex=inWarpIndex+i*warpSize;

		if(inChunkIndex>=chunkSize)
			continue;

		if(sourceDistArrayBegin[inChunkIndex]<=pivot)
			++s_L1[inWarpIndex];
		else
			++s_L2[inWarpIndex];
	}
	
	assert(s_L1[inWarpIndex]>=0&&s_L1[inWarpIndex]<chunkSize);
	assert(s_L2[inWarpIndex]>=0&&s_L2[inWarpIndex]<chunkSize);


	assert(inWarpIndex>=chunkSize||*s_pivotInd==inWarpIndex||(inWarpIndex<chunkSize&&(s_L1[inWarpIndex]>0||s_L2[inWarpIndex]>0)));
	
	assert(inWarpIndex>=chunkSize||*s_pivotInd==inWarpIndex||((s_L1[inWarpIndex]==0&&s_L2[inWarpIndex]>0)||s_L1[inWarpIndex]>0));
	assert(inWarpIndex>=chunkSize||*s_pivotInd==inWarpIndex||((s_L2[inWarpIndex]==0&&s_L1[inWarpIndex]>0)||s_L2[inWarpIndex]>0));
	
	int lVal=s_L1[inWarpIndex];
	int rVal=s_L2[inWarpIndex];

	s_L1[inWarpIndex]=0;
	s_L2[inWarpIndex]=0;
	inclusive_warp_scan(inWarpIndex, lVal, s_L1);
	inclusive_warp_scan(inWarpIndex, rVal, s_L2);
			
	assert(s_L1[32+inWarpIndex]<=chunkSize);
	assert(s_L2[32+inWarpIndex]<=chunkSize);
	assert(s_L1[63]+s_L2[63]==chunkSize);

	int destPivotInd=s_L1[63]-1;

	int l1WritePosition=(inWarpIndex==0)?0:s_L1[32+inWarpIndex-1];
	int l2WritePosition=(inWarpIndex==0)?0:s_L2[32+inWarpIndex-1];

	
	assert(destPivotInd>=0);
	assert(destPivotInd<chunkSize);
	

	l2WritePosition+=destPivotInd+1;

	for(int i=0; i<chunksInRow; ++i){
		int inChunkIndex=inWarpIndex+i*warpSize;

		if(inChunkIndex>=chunkSize)
			continue;
		
		if(sourceDistArrayBegin[inChunkIndex]<=pivot){
			assert(l1WritePosition<=destPivotInd);
						
			destDistArrayBegin[l1WritePosition]=sourceDistArrayBegin[inChunkIndex];
			destIndArrayBegin[l1WritePosition]=sourceIndicesArrayBegin[inChunkIndex];
			l1WritePosition++;
		}
		else{
			assert(l2WritePosition>destPivotInd);
			assert(l2WritePosition<chunkSize);

			destDistArrayBegin[l2WritePosition]=sourceDistArrayBegin[inChunkIndex];
			destIndArrayBegin[l2WritePosition]=sourceIndicesArrayBegin[inChunkIndex];
			l2WritePosition++;
		}
	}

	assert(destPivotInd>=0);
	assert(destPivotInd<chunkSize);

#ifndef NDEBUG
	CheckIfPartitioned(inWarpIndex, chunkSize, destPivotInd, pivot, *s_pivotInd, destDistArrayBegin);//TODO: disable in production code
#endif
	
	return destPivotInd;
}

template <class T>
inline __device__
void swap(T &lhs, T &rhs){
	T temp=rhs;
	rhs=lhs;
	lhs=temp;
}


template <class Key_T, class Values_T>
inline __device__
void BitonicWarpSortElems(int index1, int index2, volatile Key_T *s_keys, volatile Values_T *s_values){
	if(s_keys[index1]>s_keys[index2]){
		swap(s_keys[index1],s_keys[index2]);
		swap(s_values[index1],s_values[index2]);
	}
}


template <class Key_T, class Values_T>
inline __device__
void BitonicWarpSort64(int inWarpIndex, volatile Key_T *s_keys, volatile Values_T *s_values){
	

	BitonicWarpSortElems(inWarpIndex*2, inWarpIndex*2+1, s_keys, s_values);

	{
		if(inWarpIndex<warpSize/2){
			BitonicWarpSortElems(inWarpIndex*4, inWarpIndex*4+3, s_keys, s_values);
			BitonicWarpSortElems(inWarpIndex*4+1, inWarpIndex*4+2, s_keys, s_values);
		}

		BitonicWarpSortElems(inWarpIndex*2, inWarpIndex*2+1, s_keys, s_values);
	}

	{
		if(inWarpIndex<warpSize/4){
			for(int ind1=inWarpIndex*8, ind2=inWarpIndex*8+7; ind1<ind2; ++ind1, --ind2){
				BitonicWarpSortElems(ind1, ind2, s_keys, s_values);
			}
		}

		if(inWarpIndex<warpSize/2){
			for(int ind1=inWarpIndex*4, ind2=inWarpIndex*4+2; ind1<inWarpIndex*4+2; ++ind1, ++ind2){
				BitonicWarpSortElems(ind1, ind2, s_keys, s_values);
			}
		}

		BitonicWarpSortElems(inWarpIndex*2, inWarpIndex*2+1, s_keys, s_values);
	}

	{
		if(inWarpIndex<warpSize/8){
			for(int ind1=inWarpIndex*16, ind2=inWarpIndex*16+15; ind1<ind2; ++ind1, --ind2){
				BitonicWarpSortElems(ind1, ind2, s_keys, s_values);
			}
		}

		if(inWarpIndex<warpSize/4){
			for(int ind1=inWarpIndex*8, ind2=inWarpIndex*8+4; ind1<inWarpIndex*8+4; ++ind1, ++ind2){
				BitonicWarpSortElems(ind1, ind2, s_keys, s_values);
			}
		}

		if(inWarpIndex<warpSize/2){
			for(int ind1=inWarpIndex*4, ind2=inWarpIndex*4+2; ind1<inWarpIndex*4+2; ++ind1, ++ind2){
				BitonicWarpSortElems(ind1, ind2, s_keys, s_values);
			}
		}

		BitonicWarpSortElems(inWarpIndex*2, inWarpIndex*2+1, s_keys, s_values);
	}
	
	{
		if(inWarpIndex<warpSize/16){
			for(int ind1=inWarpIndex*32, ind2=inWarpIndex*32+31; ind1<ind2; ++ind1, --ind2){
				BitonicWarpSortElems(ind1, ind2, s_keys, s_values);
			}
		}

		if(inWarpIndex<warpSize/8){
			for(int ind1=inWarpIndex*16, ind2=inWarpIndex*16+8; ind1<inWarpIndex*16+8; ++ind1, ++ind2){
				BitonicWarpSortElems(ind1, ind2, s_keys, s_values);
			}
		}

		if(inWarpIndex<warpSize/4){
			for(int ind1=inWarpIndex*8, ind2=inWarpIndex*8+4; ind1<inWarpIndex*8+4; ++ind1, ++ind2){
				BitonicWarpSortElems(ind1, ind2, s_keys, s_values);
			}
		}

		if(inWarpIndex<warpSize/2){
			for(int ind1=inWarpIndex*4, ind2=inWarpIndex*4+2; ind1<inWarpIndex*4+2; ++ind1, ++ind2){
				BitonicWarpSortElems(ind1, ind2, s_keys, s_values);
			}
		}

		BitonicWarpSortElems(inWarpIndex*2, inWarpIndex*2+1, s_keys, s_values);
	}

	{
		if(inWarpIndex<warpSize/32){
			for(int ind1=inWarpIndex*64, ind2=inWarpIndex*64+63; ind1<ind2; ++ind1, --ind2){
				BitonicWarpSortElems(ind1, ind2, s_keys, s_values);
			}
		}

		if(inWarpIndex<warpSize/16){
			for(int ind1=inWarpIndex*32, ind2=inWarpIndex*32+16; ind1<inWarpIndex*32+16; ++ind1, ++ind2){
				BitonicWarpSortElems(ind1, ind2, s_keys, s_values);
			}
		}

		if(inWarpIndex<warpSize/8){
			for(int ind1=inWarpIndex*16, ind2=inWarpIndex*16+8; ind1<inWarpIndex*16+8; ++ind1, ++ind2){
				BitonicWarpSortElems(ind1, ind2, s_keys, s_values);
			}
		}

		if(inWarpIndex<warpSize/4){
			for(int ind1=inWarpIndex*8, ind2=inWarpIndex*8+4; ind1<inWarpIndex*8+4; ++ind1, ++ind2){
				BitonicWarpSortElems(ind1, ind2, s_keys, s_values);
			}
		}

		if(inWarpIndex<warpSize/2){
			for(int ind1=inWarpIndex*4, ind2=inWarpIndex*4+2; ind1<inWarpIndex*4+2; ++ind1, ++ind2){
				BitonicWarpSortElems(ind1, ind2, s_keys, s_values);
			}
		}

		BitonicWarpSortElems(inWarpIndex*2, inWarpIndex*2+1, s_keys, s_values);
	}

	//assert(s_keys[inWarpIndex*2]<=s_keys[inWarpIndex*2+1]);
	//assert(inWarpIndex==0||s_keys[inWarpIndex*2-1]<=s_keys[inWarpIndex*2]);
/*	if(inWarpIndex!=0&&s_keys[inWarpIndex*2-1]>s_keys[inWarpIndex*2]){
		printf("%d %e %e\n", inWarpIndex, s_keys[inWarpIndex*2-1],s_keys[inWarpIndex*2]);
		for(int i=0; i<64; ++i){
			printf("%d %d %e\n", inWarpIndex, i, s_keys[i]);
		}
	}*/
}

__device__ inline
void partitionToShared(int inWarpIndex, int ballot, int tailSizeFor32, int index, float value,
	volatile int *s_indices, volatile float *s_dist)
{
	int onesBefore=__popc(ballot<<(32-inWarpIndex));
	
	int pos;
	if(ballot & (1 << inWarpIndex)){
		pos=onesBefore;
	}else{
		pos=tailSizeFor32-(inWarpIndex-onesBefore)-1;
	}
	assert(pos>=0);
	assert(pos<32);
	s_indices[pos]=index;
	s_dist[pos]=value;
}

__forceinline__ __device__
void sharedToGlobal(int inWarpIndex, int toLeftCount, int toRightCount, int totalToLeftCount, int kInternalInd,
	 volatile const int *s_indices,  volatile float const *s_dist,
	 int leftWritePos,  int rightWritePos,
	 int *distIndicesArrayBegin, float *distDestArrayBegin)
{
	int pos=inWarpIndex;
	if(inWarpIndex<toLeftCount){
		pos+=leftWritePos;
	}else{
		pos+=rightWritePos-(toLeftCount+toRightCount);
	}
	if(inWarpIndex<toLeftCount||kInternalInd>=totalToLeftCount)//prevent writing unused memory
	{
		distIndicesArrayBegin[pos]=s_indices[inWarpIndex];
		distDestArrayBegin[pos]=s_dist[inWarpIndex];
	}
}

__forceinline__ __device__ 
int PartitionBallot(int rowIndex, int inWarpIndex,
	int warpInBlockInd,
	volatile int *s_pivotInd,
	float *sourceDistArrayBegin, const float *sourceDistArrayEnd,
	int *sourceIndicesArrayBegin,	
	float *destDistArrayBegin,
	int *destIndArrayBegin, 
	int kInternalInd,
	RNG &rng)
{
	int chunkSize=sourceDistArrayEnd-sourceDistArrayBegin;

#define ilpLevel 4

	int chunksInRow=DivUp(chunkSize,warpSize*ilpLevel);
	
	bool qq=false;
	if(inWarpIndex==0){
		*s_pivotInd=rng.intInRange(0, chunkSize-1, rowIndex);
		if(chunkSize==2&&sourceDistArrayBegin[0]==sourceDistArrayBegin[1])
		{
			//printf("Changing %d to 1\n",*s_pivotInd);
			*s_pivotInd=1;
			qq=true;
		}
		//printf("pivot index selected:  %d; pivotElement:%e\n", *s_pivotInd, sourceDistArrayBegin[*s_pivotInd]);
	}

	assert(kInternalInd>=0);
	assert(kInternalInd<chunkSize);

	__shared__ int s_indicesConmmon[ilpLevel*32*warpsPerBlock];
	__shared__ float s_distCommon[ilpLevel*32*warpsPerBlock];

	volatile int *s_indices1=s_indicesConmmon+warpInBlockInd*ilpLevel*warpSize;
	volatile float *s_dist1=s_distCommon+warpInBlockInd*ilpLevel*warpSize;
	volatile int *s_indices2=s_indicesConmmon+warpInBlockInd*ilpLevel*warpSize+32;
	volatile float *s_dist2=s_distCommon+warpInBlockInd*ilpLevel*warpSize+32;
	volatile int *s_indices3=s_indicesConmmon+warpInBlockInd*ilpLevel*warpSize+64;
	volatile float *s_dist3=s_distCommon+warpInBlockInd*ilpLevel*warpSize+64;
	volatile int *s_indices4=s_indicesConmmon+warpInBlockInd*ilpLevel*warpSize+96;
	volatile float *s_dist4=s_distCommon+warpInBlockInd*ilpLevel*warpSize+96;

	assert(*s_pivotInd>=0&&*s_pivotInd<chunkSize);
	float pivot=sourceDistArrayBegin[*s_pivotInd];

	//these two can be put into registers, but it didn't bring any good...
	__shared__ int s_leftWritePos[warpsPerBlock];
	__shared__ int s_rightWritePos[warpsPerBlock];

	
	if(inWarpIndex==0){
		s_leftWritePos[warpInBlockInd]=0;
		s_rightWritePos[warpInBlockInd]=chunkSize;
	}

	int i=0;
	for(; i<chunksInRow-1; ++i){
		int inChunkIndex1=inWarpIndex+i*ilpLevel*warpSize;
		int inChunkIndex2=inWarpIndex+i*ilpLevel*warpSize+warpSize;
		int inChunkIndex3=inWarpIndex+i*ilpLevel*warpSize+2*warpSize;
		int inChunkIndex4=inWarpIndex+i*ilpLevel*warpSize+3*warpSize;

		float t_value1=sourceDistArrayBegin[inChunkIndex1];
		float t_value2=sourceDistArrayBegin[inChunkIndex2];
		float t_value3=sourceDistArrayBegin[inChunkIndex3];
		float t_value4=sourceDistArrayBegin[inChunkIndex4];
		int t_index1=sourceIndicesArrayBegin[inChunkIndex1];
		int t_index2=sourceIndicesArrayBegin[inChunkIndex2];
		int t_index3=sourceIndicesArrayBegin[inChunkIndex3];
		int t_index4=sourceIndicesArrayBegin[inChunkIndex4];

		int B1=__ballot(t_value1<=pivot);
		int B2=__ballot(t_value2<=pivot);
		int B3=__ballot(t_value3<=pivot);
		int B4=__ballot(t_value4<=pivot);
		
		int toLeftCount1=__popc(B1);
		int toLeftCount2=__popc(B2);
		int toLeftCount3=__popc(B3);
		int toLeftCount4=__popc(B4);
		int toRightCount1=warpSize-toLeftCount1;
		int toRightCount2=warpSize-toLeftCount2;
		int toRightCount3=warpSize-toLeftCount3;
		int toRightCount4=warpSize-toLeftCount4;

		partitionToShared(inWarpIndex, B1, warpSize, t_index1, t_value1, s_indices1, s_dist1);
		partitionToShared(inWarpIndex, B2, warpSize, t_index2, t_value2, s_indices2, s_dist2);
		partitionToShared(inWarpIndex, B3, warpSize, t_index3, t_value3, s_indices3, s_dist3);
		partitionToShared(inWarpIndex, B4, warpSize, t_index4, t_value4, s_indices4, s_dist4);
			
		sharedToGlobal(inWarpIndex, toLeftCount1, toRightCount1, s_leftWritePos[warpInBlockInd]+toLeftCount1, kInternalInd, 
			s_indices1, s_dist1,
			s_leftWritePos[warpInBlockInd], s_rightWritePos[warpInBlockInd],
			destIndArrayBegin, destDistArrayBegin);	
	
		sharedToGlobal(inWarpIndex, toLeftCount2, toRightCount2, s_leftWritePos[warpInBlockInd]+toLeftCount2+toLeftCount1, kInternalInd, 
			s_indices2, s_dist2,
			s_leftWritePos[warpInBlockInd]+toLeftCount1, s_rightWritePos[warpInBlockInd]-toRightCount1,
			destIndArrayBegin, destDistArrayBegin);	

		sharedToGlobal(inWarpIndex, toLeftCount3, toRightCount3, s_leftWritePos[warpInBlockInd]+toLeftCount1+toLeftCount2+toLeftCount3, kInternalInd, 
			s_indices3, s_dist3,
			s_leftWritePos[warpInBlockInd]+toLeftCount1+toLeftCount2, s_rightWritePos[warpInBlockInd]-toRightCount1-toRightCount2,
			destIndArrayBegin, destDistArrayBegin);	

		sharedToGlobal(inWarpIndex, toLeftCount4, toRightCount4, s_leftWritePos[warpInBlockInd]+toLeftCount1+toLeftCount2+toLeftCount3+toLeftCount4, kInternalInd, 
			s_indices4, s_dist4,
			s_leftWritePos[warpInBlockInd]+toLeftCount1+toLeftCount2+toLeftCount3, s_rightWritePos[warpInBlockInd]-toRightCount1-toRightCount2-toRightCount3,
			destIndArrayBegin, destDistArrayBegin);	

			
//		assert(tailSize>warpSize);

		if(inWarpIndex==0){
			s_leftWritePos[warpInBlockInd]+=toLeftCount4+toLeftCount3+toLeftCount2+toLeftCount1;
			s_rightWritePos[warpInBlockInd]-=toRightCount4+toRightCount3+toRightCount2+toRightCount1;
		}
		
	}//for whole chunks (of 64 elements)

	//__shared__ int s_totalToLeftCount[warpsPerBlock];

	//if(inWarpIndex==0){
	//	s_totalToLeftCount[warpInBlockInd]=s_leftWritePos[warpInBlockInd];
	//}
	//last chunk, divergence is possible
	
	int tailSize=chunkSize%(ilpLevel*warpSize);
	if(tailSize==0)
		tailSize=ilpLevel*warpSize;

	chunksInRow=DivUp(tailSize,warpSize);
	for(int j=0; j<chunksInRow; ++j)
	{
		int warpShift=i*(ilpLevel*warpSize)+j*warpSize;
		int inChunkIndex=inWarpIndex+warpShift;

		int currTailSize=min(chunkSize-warpShift, warpSize);
		assert(currTailSize>0);
		
		float t_value=inChunkIndex<chunkSize?sourceDistArrayBegin[inChunkIndex]:FLT_MAX;
		int t_index=inChunkIndex<chunkSize?sourceIndicesArrayBegin[inChunkIndex]:0;
		
		int B=__ballot(t_value<=pivot);
				
		int toLeftCount=__popc(B);
		assert(toLeftCount>=0);
		assert(toLeftCount<=32);
		int toRightCount=currTailSize-toLeftCount;
		assert(toRightCount>=0);
		assert(toRightCount<=32);
		
		
		if(inChunkIndex<chunkSize)
		{
			partitionToShared(inWarpIndex, B, currTailSize, t_index, t_value, s_indices1, s_dist1);
		
			sharedToGlobal(inWarpIndex, toLeftCount, toRightCount, s_leftWritePos[warpInBlockInd]+toLeftCount, kInternalInd, 
				s_indices1, s_dist1,
				s_leftWritePos[warpInBlockInd], s_rightWritePos[warpInBlockInd],
				destIndArrayBegin, destDistArrayBegin);	
	
			if(inWarpIndex==0){
				s_leftWritePos[warpInBlockInd]+=toLeftCount;
				s_rightWritePos[warpInBlockInd]-=toRightCount;
			}

		}
	
	}
	#ifndef NDEBUG
		CheckIfPartitioned(inWarpIndex, chunkSize, s_leftWritePos[warpInBlockInd]-1, pivot, *s_pivotInd, destDistArrayBegin);//TODO: disable in production code
	#endif

		if(qq==true){
			++s_leftWritePos[warpInBlockInd];
			//printf("%d\n",s_leftWritePos[warpInBlockInd]-1);
		}
	
	return s_leftWritePos[warpInBlockInd]-1;

}



inline __device__ 
void searchByThread(float valueToFind, int inValuesIndex, float const* rowExtractedDist, int *rowIndiciesStart, int length)
{
	/*int firstInd=0;
	int lastInd=length-1;

	if(threadIdx.x==9)
		printf("%e %e %e %e\n", rowExtractedDist[0], rowExtractedDist[1],  rowExtractedDist[2],  rowExtractedDist[3]);

	for(int i=0; i<100; ++i)
	{
		int ind=(firstInd+lastInd)/2;

		

		float kValue=rowExtractedDist[ind];
		if(threadIdx.x==9)
		printf("%d %d %d %e %e %d\n", firstInd, ind, lastInd, kValue, valueToFind, threadIdx.x);
		if(kValue==valueToFind){
			rowIndiciesStart[ind]=inValuesIndex;
			printf("Found by thread %d\n",  threadIdx.x);
			break;
		}

		if(firstInd>=lastInd)
			break;
		
		if(kValue>valueToFind){
			lastInd=ind;
		}else{
			firstInd=ind+1;
		}
	}*/

	for(int i=0; i<length; ++i){
			float kValue=rowExtractedDist[i];

			if(kValue==valueToFind)
				rowIndiciesStart[i]=inValuesIndex;
			
	}
}

__global__ 
void ExtractIndicesKernel(float const *sourceDist, float const *extractedDist,
	int *indicies,
	CLArguments const clArgs)
{
	int const index=threadIdx.x+blockIdx.x*blockDim.x;
	int const inWarpIndex=index%warpSize;
	int const rowIndex=index/warpSize;
	if(rowIndex>=clArgs.numberOfRows)
		return;

	int const valuesChunksInRow=DivUp(clArgs.numberOfColumns,warpSize);
//	int const warpInBlockInd=rowIndex%warpsPerBlock;

	const int shift=rowIndex*clArgs.numberOfColumns;

	float const *rowDistStart=sourceDist+shift;
	float const *rowExtractedDist=extractedDist+shift;
	int *rowIndiciesStart=indicies+shift;

	//__shared__ bool s_found[warpsPerBlock];
	for(int valInd=0; valInd<valuesChunksInRow; ++valInd){
		int inValuesIndex=valInd*warpSize+inWarpIndex;
		if(inValuesIndex>=clArgs.numberOfColumns)
			break;
		float threadValue=rowDistStart[inValuesIndex];


		searchByThread(threadValue, inValuesIndex, rowExtractedDist, rowIndiciesStart, clArgs.k);
		
	}

	//int const warpInBlockInd=rowIndex%warpsPerBlock;

	/*int valuesChunksInRow=DivUp(clArgs.numberOfColumns,warpSize);
	int ksChunksInRow=DivUp(clArgs.k,warpSize);
	for(int chInd=0; chInd<=ksChunksInRow; ++chInd){
		int inKeysIndex=chInd*warpSize+inWarpIndex;
		if(inKeysIndex>=clArgs.k)
			return;

		float valueToFind=extractedDist[inKeysIndex];

		printf("Looking for a value %e tId: %d\n", valueToFind, index);
		for(int valInd=0; valInd<valuesChunksInRow; ++valInd){
			int inValuesIndex=valInd*warpSize+inWarpIndex;
			if(inValuesIndex<clArgs.numberOfColumns){
				printf("Looking for a value %e, current value is %e, tId: %d\n", valueToFind, sourceDist[inValuesIndex], index);
				if(sourceDist[inValuesIndex]==valueToFind){
					printf("Looking for a value %e, current value is %e, found at index %d, tId: %d\n", valueToFind, sourceDist[inValuesIndex], inValuesIndex, index);
					indicies[inKeysIndex]=inValuesIndex;
					break;
				}
			}
		}
	}
	printf("WTF I'm doing here?\n");*/
}

/*
__device__ __inline__
void countMatchingForWarpsInChunk(int index, int rowInd, int inWarpIndex, float const *chunkValues, float pivotVal, bool inRange, int *s_matchingForRow){
	
	int bl=ballot(inRange&&(chunkValues[index]<=pivotVal));

	int toLeft=__popc(bl);

	if(inWarpIndex==0)
		s_matchingForRow[inWarpIndex]+=toLeft;

	//printf("thread #%3d, warp #%d, th. in warp index #%2d, block #%d, rng ind: %d, rnd: %5.2e, ballot: %6d, to left: %d\n", 
		//index, warpIndex, threadInWarpIndex, blockIndex, rvi, rv, bl, toLeft);

}*/

__device__ __inline__
void countMatchingForWarpsInRow(int index, int rowInd, int rowLength, int inWarpIndex, int warpIndex, float const *rowValues, float pivotVal, int *s_matchingForRow){
	int chunksInRow=DivUp(rowLength, blockDim.x);//TODO: extract?

	if(inWarpIndex==0){
		s_matchingForRow[warpIndex]=0;
		printf("Chunks in a row: %d, pivot: %5.2e\n", chunksInRow, pivotVal);
	}

	int elemsLeft=rowLength;
	for(int ch=0; ch<chunksInRow; ++ch){
		int chunkSize=min(elemsLeft, warpSize);
		
		{//countMatchingForWarpsInChunk(index, rowInd, warpIndex, rowValues, pivotVal, index<rowLength, s_matchingForRow);

			unsigned int bl=ballot((index<elemsLeft)&&(rowValues[index]<=pivotVal));

			int toLeft=__popc(bl);

			if(inWarpIndex==0)
				s_matchingForRow[warpIndex]+=toLeft;

			if(inWarpIndex==0){
				printf("thread #%3d, warp #%d, chunk: %d, block #%d, rnd: %5.2e, ballot: %6u, to left: %d\n", 
					index, warpIndex, ch, rowInd, pivotVal, bl, toLeft);
			}
		}

		elemsLeft-=blockDim.x;
		rowValues+=blockDim.x;
	}
}

//prefix sum for writing positions. Probably, could be parallelized 
__device__ __inline__
void prefixSumIterative(unsigned int  *arr, int len)
{
	//assert(threadIdx.x==0);
	int prev=arr[0];
	arr[0]=0;
	for(int i=1; i<len; ++i){
		int curr=arr[i];
		arr[i]=prev+arr[i-1];
		//printf("%d: arr[%d]=%d\n", threadIdx.x, i, arr[i]);
		prev=curr;
	}
}

__device__ __inline__
unsigned int toWriteGlobIndex(unsigned int inWarpIndex, unsigned int warpIndex, unsigned int w_beginIndex,  unsigned int w_toLeft, unsigned int w_toRight,
	unsigned int b_beginIndex, unsigned int b_endIndex)
{
	int toLeftInd=b_beginIndex+w_beginIndex+inWarpIndex;//store before doing prefix sum
	int toRightWarpIndexRHS=b_endIndex-(warpIndex*32-w_beginIndex);
	int toRightWarpIndexLHS=toRightWarpIndexRHS-w_toRight;
	int toRightInd=toRightWarpIndexLHS+(inWarpIndex-w_toLeft);
	unsigned int toWriteInGlobalInd=(inWarpIndex<w_toLeft)?toLeftInd:toRightInd;
	//if(warpIndex==1)
	//if(inWarpIndex<w_toLeft)
	//printf("warp index: %u, in warp index: %2u, to left in warp: %d, to left? %u, warp start: %2d, tRWILHS: %2d, tRWIRHS: %2d, global ind: %2u, to left ind: %2u, to right ind: %2u, to right: %2d\n", 
		//warpIndex, inWarpIndex, w_toLeft, inWarpIndex<w_toLeft, b_beginIndex+w_beginIndex, toRightWarpIndexLHS, toRightWarpIndexRHS, toWriteInGlobalInd, toLeftInd, toRightInd, w_toRight);

	return toWriteInGlobalInd;
}

__device__ __inline__
unsigned int toWritesharedIndex(int inWarpIndex, int warpIndex, unsigned int bl, bool t_doesGoToLeft, unsigned int chunkSize)
{
		unsigned int beforeMeOnLeft=__popc(bl&~(0xFFFFFFFF<<inWarpIndex));
		assert(beforeMeOnLeft<=inWarpIndex);
		unsigned int beforeMeOnRight=inWarpIndex-beforeMeOnLeft;
		
		//uncoalesced writing to shared memory
		unsigned int toWriteInSharedInd=(32*warpIndex)+(t_doesGoToLeft?beforeMeOnLeft:(chunkSize-1-beforeMeOnRight));
		//if(warpIndex==1)
			//printf("-----------------------%d, %u\n", chunkSize-1-beforeMeOnRight, toWriteInSharedInd);
		//assert(toWriteInSharedInd<chunkSize);
		return toWriteInSharedInd;
}




/*template <typename T>
__device__ __inline__
float value(T arrPtrs, unsigned ind);*/

struct KeyIndex{
	float key;
	int index;

	__device__ __inline__
	KeyIndex(){}

	__device__ __inline__
	KeyIndex(float k, int i):key(k), index(i){}
};

template <typename T>
struct Traits;

template <>
struct Traits<KeyIndexArray>{
	typedef KeyIndex Elem_t;
};


template <>
struct Traits<float *>{
	typedef float Elem_t;
};

template <typename T>
__device__ __inline__
Traits<KeyIndexArray>::Elem_t get(T arrPtrs, unsigned ind){//TODO: do we need it?
	return Traits<KeyIndexArray>::Elem_t(*(arrPtrs.values+ind),*(arrPtrs.indices+ind));
}

__device__ __inline__
float get(float * arrPtrs, unsigned ind){//TODO: do we need it?
	return *(arrPtrs+ind);
}

template <typename T>
__device__ __inline__
bool isValidValue(T arrPtrs, unsigned ind);//TODO: delagate to "get" function?


template <>
__device__ __inline__
bool isValidValue(KeyIndexArray arrPtrs, unsigned ind){
	return (arrPtrs.values[ind]<1E+36)&&(arrPtrs.values[ind]>-1E+36);
}

__device__ __inline__
bool isValidValue(float * arrPtrs, unsigned ind){
	return arrPtrs[ind]<1E+36&&arrPtrs[ind]>-1E+36;
}


template <typename T>
__device__ __inline__
bool goesToLeft(T arg, float pivotVal);

template <>
__device__ __inline__
bool goesToLeft(Traits<KeyIndexArray>::Elem_t arg, float pivotVal){
	return arg.key<=pivotVal;
}

__device__ __inline__
bool goesToLeft(float arg, float pivotVal){
	return arg<=pivotVal;
}

/*
__device__ __inline__
void shift(Temp arrToWrite, unsigned int shift){
	arrToWrite.indices+=shift;
	arrToWrite.values+=shift;
}
*/
__device__ __inline__
void copy(KeyIndexArray arrToWrite, unsigned int ind, Traits<KeyIndexArray>::Elem_t elem){
	arrToWrite.indices[ind]=elem.index;
	arrToWrite.values[ind]=elem.key;
}

/*
__device__ __inline__//TODO: fix here
void copy(Traits<KeyIndexArray>::Elem_t *arrToWrite, unsigned int ind, Traits<KeyIndexArray>::Elem_t elem){
	Traits<KeyIndexArray>::Elem_t *currElem=arrToWrite+ind;
	currElem->key=elem.key;
	currElem->index=elem.index;
	
}*/

__device__ __inline__//TODO: fix here
void copy(KeyIndexArray arrToWrite, unsigned int ind, KeyIndexArray fromArr, unsigned int indFrom){
	arrToWrite.values[ind]=fromArr.values[indFrom];
	arrToWrite.indices[ind]=fromArr.indices[indFrom];
}

__device__ __inline__//TODO: fix here
void copy(float *arrToWrite, unsigned int ind, float *fromArr, unsigned int indFrom){
	*(arrToWrite+ind)=*(fromArr+indFrom);
}

__device__ __inline__
void assign(KeyIndexArray &ki, char *buff, int len){
	ki.values=(float*)buff;
	ki.indices=(int *)ki.values+len;
}

__device__ __inline__
void assign(float *&ki, char *buff, int len){
	ki=(float*)buff;
}

__device__ __inline__
void copy(float *arrToWrite, unsigned int ind, float elem){
	arrToWrite[ind]=elem;
}

__device__ __inline__
float value(KeyIndexArray arg, unsigned int ind){
	return arg.values[ind];
}

__device__ __inline__
float value(float *arg, unsigned int ind){
	return arg[ind];
}

__device__ __inline__
float value(KeyIndex arg){
	return arg.key;
}

__device__ __inline__
float value(float arg){
	return arg;
}



/*template <typename T>
__device__ __inline__
bool goesToLeft();*/

/*
template <bool>
struct Temp;

template <>
struct Temp<true>{
	float const *values;
	int const *indices;
	const float values
};

template <>
struct Temp<false>{
	float *values;
	int  *indices;
};


template <typename T>
__device__ __inline__
bool idValidValue(T arrPtrs, unsigned ind);

template <>
__device__ __inline__
bool idValidValue<Temp<true> >(Temp<true> arg, unsigned ind){
	return (arg.values[ind]<1E+36)&&(arg.values[ind]>-1E+36);
}*/


/*template <>
__device__ __inline__
bool idValidValue<>(float value){
	return (value<1E+36)&&(value>-1E+36);
}*/

/*template <typename T>
__device__ __inline__
bool idValidValue(Temp<T> arg){
	return (arg.value<1E+36)&&(arg.value>-1E+36);
}*/

//__device__ __constant__ unsigned int gd_warpsPB;//HACK: weird usage, but doesn't work otherwise.

//__device__ __constant__ const unsigned int gd_warpsPBConst=12;

template <typename T, int warpsPB>
__device__ __inline__
unsigned int splitByPivot(int index, int rowInd, int rowLength, int inWarpIndex, int warpIndex, float pivotVal, 
	T sourceArrs, T splitArrs)
{
	typedef typename Traits<T>::Elem_t Elem_t;
	
	int chunksInRow=rowLength/(blockDim.x*ILP_LEVEL);

	/*if(index==0)
		for(int i=0; i<rowLength; ++i){//TODO: comment or delete!
			assert(isValidValue(sourceArrs, i));
		}*/
	
	int movedToLeft=0;

	extern __shared__ char buff[];

	unsigned int *s_toLeftByWarp=(unsigned int *)buff;
	//__shared__ unsigned int s_toLeftByWarp[(gd_warpsPBConst+1)*ILP_LEVEL];
	//__shared__ char ss_ki[32*gd_warpsPBConst*sizeof(Elem_t)];
	T s_ki;
	assign(s_ki,(char*)(s_toLeftByWarp+(warpsPB+1)*ILP_LEVEL),warpsPB*warpSize);

	
	if(index<(warpsPB+1)*4)
			s_toLeftByWarp[index]=0;
	__syncthreads();

	
	int elemsLeft=rowLength;
	int endIndex=rowLength;
	int beginIndex=0;
	int shift=0;
	for(int ch=0; ch<chunksInRow; ++ch){

		/*if(index<(warpsPB+1)*4)
			s_toLeftByWarp[index]=0;
		__syncthreads();*/


		//if(elemsLeft<blockDim.x*4)
			//printf("here: %d %d %d\n", ch, elemsLeft, blockDim.x*4);
		assert(elemsLeft>=blockDim.x*4);

		const int inGlobIndex0=shift+index;
		const int inGlobIndex1=inGlobIndex0+blockDim.x;
		const int inGlobIndex2=inGlobIndex1+blockDim.x;
		const int inGlobIndex3=inGlobIndex2+blockDim.x;


		//if(inChunkIndex0>=elemsLeft){
			//printf("%d %d %d %d %d\n", inChunkIndex0, elemsLeft, ch, chunksInRow, shift);
		//}
		assert(inGlobIndex0<rowLength);
		assert(inGlobIndex1<rowLength);
		assert(inGlobIndex2<rowLength);
		assert(inGlobIndex3<rowLength);
		
		//caching current elements to avoid rereading them from global memory
		typename Traits<T>::Elem_t t_currElem0=get(sourceArrs, inGlobIndex0);
		typename Traits<T>::Elem_t t_currElem1=get(sourceArrs, inGlobIndex1);
		typename Traits<T>::Elem_t t_currElem2=get(sourceArrs, inGlobIndex2);
		typename Traits<T>::Elem_t t_currElem3=get(sourceArrs, inGlobIndex3);

		bool doesGoToLeft0=goesToLeft(t_currElem0, pivotVal);
		bool doesGoToLeft1=goesToLeft(t_currElem1, pivotVal);
		bool doesGoToLeft2=goesToLeft(t_currElem2, pivotVal);
		bool doesGoToLeft3=goesToLeft(t_currElem3, pivotVal);

	
		//	printf("chunk: %d, index: %2u, warp index: %u, in warp index: %2u, to left? %u\n", 
		//		ch, index, warpIndex, inWarpIndex, doesGoToLeft);

		//need all threads here
		unsigned int bl0=ballot(doesGoToLeft0);
		unsigned int bl1=ballot(doesGoToLeft1);
		unsigned int bl2=ballot(doesGoToLeft2);
		unsigned int bl3=ballot(doesGoToLeft3);

		assert(warpIndex<warpsPB);


		int w_toLeft0=__popc(bl0);
		int w_toLeft1=__popc(bl1);
		int w_toLeft2=__popc(bl2);
		int w_toLeft3=__popc(bl3);

		if(inWarpIndex==0){
			s_toLeftByWarp[warpIndex]=w_toLeft0;
			assert(s_toLeftByWarp[warpIndex]<=32);

			s_toLeftByWarp[1*(warpsPB+1)+warpIndex]=w_toLeft1;
			assert(s_toLeftByWarp[1*(warpsPB+1)+warpIndex]<=32);

			s_toLeftByWarp[2*(warpsPB+1)+warpIndex]=w_toLeft2;
			assert(s_toLeftByWarp[2*(warpsPB+1)+warpIndex]<=32);

			s_toLeftByWarp[3*(warpsPB+1)+warpIndex]=w_toLeft3;
			assert(s_toLeftByWarp[3*(warpsPB+1)+warpIndex]<=32);
			
		}
		

		//assert(false);

		//store before doing prefix sum
		//int w_toLeft0=s_toLeftByWarp[0*(warpsPB+1)+warpIndex];
		assert(w_toLeft0>=0);
		assert(w_toLeft0<=warpSize);
		int w_toRight0=warpSize-w_toLeft0;
		assert(w_toRight0>=0);
		assert(w_toRight0<=warpSize);

		//int w_toLeft1=s_toLeftByWarp[1*(warpsPB+1)+warpIndex];
		assert(w_toLeft1>=0);
		assert(w_toLeft1<=warpSize);
		int w_toRight1=warpSize-w_toLeft1;
		assert(w_toRight1>=0);
		assert(w_toRight1<=warpSize);

		//int w_toLeft2=s_toLeftByWarp[2*(warpsPB+1)+warpIndex];
		if(w_toLeft2>warpSize)
		assert(w_toLeft2>=0);
		assert(w_toLeft2<=warpSize);
		int w_toRight2=warpSize-w_toLeft2;
		assert(w_toRight2>=0);
		assert(w_toRight2<=warpSize);

	//	int w_toLeft3=s_toLeftByWarp[3*(warpsPB+1)+warpIndex];
		assert(w_toLeft3>=0);
		assert(w_toLeft3<=warpSize);
		int w_toRight3=warpSize-w_toLeft3;
		assert(w_toRight3>=0);
		assert(w_toRight3<=warpSize);

		
		__syncthreads();
		if(index<4){
			prefixSumIterative(s_toLeftByWarp+index*(warpsPB+1), warpsPB+1);
		}
		
				
		//uncoalesced writing to shared memory
		//TODO: check if interleaving with writibng to global memory helps
		unsigned int toWriteInSharedInd=toWritesharedIndex(inWarpIndex, warpIndex, bl0, doesGoToLeft0, warpSize);
		assert(toWriteInSharedInd<warpSize*warpsPB);
		copy(s_ki, toWriteInSharedInd, t_currElem0);
				
		__syncthreads();

		//coalesced writing to global memory
		unsigned int toWriteInGlobalInd=toWriteGlobIndex(inWarpIndex, warpIndex, s_toLeftByWarp[0*(warpsPB+1)+warpIndex], w_toLeft0, w_toRight0, beginIndex, endIndex);
		assert(toWriteInGlobalInd<rowLength);
		endIndex-=32*warpsPB-s_toLeftByWarp[0*(warpsPB+1)+warpsPB];
		beginIndex+=s_toLeftByWarp[0*(warpsPB+1)+warpsPB];
		int inSharedInd=warpIndex*32+inWarpIndex;
		copy(splitArrs, toWriteInGlobalInd, s_ki, inSharedInd);

		toWriteInSharedInd=toWritesharedIndex(inWarpIndex, warpIndex, bl1, doesGoToLeft1, warpSize);
		assert(toWriteInSharedInd<warpSize*warpsPB);
		copy(s_ki,toWriteInSharedInd,t_currElem1);

		toWriteInGlobalInd=toWriteGlobIndex(inWarpIndex, warpIndex, s_toLeftByWarp[1*(warpsPB+1)+warpIndex], w_toLeft1, w_toRight1, beginIndex, endIndex);
		assert(toWriteInGlobalInd<rowLength);
		endIndex-=32*warpsPB-s_toLeftByWarp[1*(warpsPB+1)+warpsPB];
		beginIndex+=s_toLeftByWarp[1*(warpsPB+1)+warpsPB];
		inSharedInd=warpIndex*32+inWarpIndex;
		copy(splitArrs, toWriteInGlobalInd, s_ki, inSharedInd);


		toWriteInSharedInd=toWritesharedIndex(inWarpIndex, warpIndex, bl2, doesGoToLeft2, warpSize);
		assert(toWriteInSharedInd<warpSize*warpsPB);
		copy(s_ki, toWriteInSharedInd, t_currElem2);

		toWriteInGlobalInd=toWriteGlobIndex(inWarpIndex, warpIndex, s_toLeftByWarp[2*(warpsPB+1)+warpIndex], w_toLeft2, w_toRight2, beginIndex, endIndex);
		assert(toWriteInGlobalInd<rowLength);
		endIndex-=32*warpsPB-s_toLeftByWarp[2*(warpsPB+1)+warpsPB];
		beginIndex+=s_toLeftByWarp[2*(warpsPB+1)+warpsPB];
		inSharedInd=warpIndex*32+inWarpIndex;
		copy(splitArrs, toWriteInGlobalInd, s_ki, inSharedInd);


		toWriteInSharedInd=toWritesharedIndex(inWarpIndex, warpIndex, bl3, doesGoToLeft3, warpSize);
		assert(toWriteInSharedInd<warpSize*warpsPB);
		copy(s_ki, toWriteInSharedInd, t_currElem3);

		toWriteInGlobalInd=toWriteGlobIndex(inWarpIndex, warpIndex, s_toLeftByWarp[3*(warpsPB+1)+warpIndex], w_toLeft3, w_toRight3, beginIndex, endIndex);
		assert(toWriteInGlobalInd<rowLength);
		endIndex-=32*warpsPB-s_toLeftByWarp[3*(warpsPB+1)+warpsPB];
		beginIndex+=s_toLeftByWarp[3*(warpsPB+1)+warpsPB];
		inSharedInd=warpIndex*32+inWarpIndex;
		copy(splitArrs, toWriteInGlobalInd, s_ki, inSharedInd);


		elemsLeft-=blockDim.x*ILP_LEVEL;
		shift+=blockDim.x*ILP_LEVEL;

		movedToLeft+=s_toLeftByWarp[0*(warpsPB+1)+warpsPB];
		movedToLeft+=s_toLeftByWarp[1*(warpsPB+1)+warpsPB];
		movedToLeft+=s_toLeftByWarp[2*(warpsPB+1)+warpsPB];
		movedToLeft+=s_toLeftByWarp[3*(warpsPB+1)+warpsPB];

	}//ch



	assert(elemsLeft>=0);
	assert(elemsLeft<ilpLevel*blockDim.x); 
	chunksInRow=DivUp(elemsLeft, blockDim.x);
	
	for(int ch=0; ch<chunksInRow; ++ch){
		int w_chunkSize=max(min(elemsLeft-warpSize*warpIndex, warpSize), 0);
		assert(w_chunkSize>=0);
		assert(w_chunkSize<=warpSize);
		
		bool validIndex=index<elemsLeft;

		typename Traits<T>::Elem_t t_currElem=get(sourceArrs, shift+index);//caching current element to avoid rereading it from global memory
		bool doesGoToLeft=validIndex&&goesToLeft(t_currElem, pivotVal);

		/*if(validIndex&&doesGoToLeft){
			assert(value(sourceArrs, shift+index)<=pivotVal);
			if(warpIndex==1){
				printf("\t***** %6.4e\n", value(sourceArrs, shift+index));
			}
		}*/


	
		/*if(validIndex)
			printf("chunk: %d, index: %2u, warp index: %u, in warp index: %2u, to left? %u\n", 
				ch, index, warpIndex, inWarpIndex, doesGoToLeft);*/

		unsigned int bl=ballot(doesGoToLeft);//need all threads here
		 

		if(inWarpIndex==0){
				s_toLeftByWarp[warpIndex]=validIndex?__popc(bl):0;
				assert(s_toLeftByWarp[warpIndex]<=32);
				//printf("Warp: %d, ballot: %u, to left: %u, chank: %d\n", warpIndex, bl, s_toLeftByWarp[warpIndex], ch);
		}
		__syncthreads();//need all threads here

		int w_toLeft, w_toRight;
		if(validIndex){
			w_toLeft=s_toLeftByWarp[warpIndex];//store before doing prefix sum
			assert(w_toLeft>=0);
			assert(w_toLeft<=warpSize);

			w_toRight=w_chunkSize-w_toLeft;//not necessarily 32-toLeft
			assert(w_toRight>=0);
			assert(w_toRight<=warpSize);
		}

		


	/*	if(inWarpIndex==0){
			printf("iteration %d, warp %d: moving %d to left; thread: %d, begin index: %d, end index: %d\n", 
				ch, warpIndex, s_toLeftByWarp[warpIndex], threadIdx.x, beginIndex, endIndex);
			//printf("warp: %d\n", warpIndex);
			
			
		}*/

		if(index==0){
			/*for(int i=0; i<warpsPB; ++i){
				printf("%d %d\n", i, s_toLeftByWarp[i]);
			}*/
			prefixSumIterative(&s_toLeftByWarp[0], warpsPB+1);
		}
		
		__syncthreads();

		if(validIndex){
			//uncoalesced writing to shared memory
			unsigned int toWriteInSharedInd=toWritesharedIndex(inWarpIndex, warpIndex, bl, doesGoToLeft, w_chunkSize);
			assert(toWriteInSharedInd<warpSize*warpsPB);
			if(toWriteInSharedInd-32*warpIndex<(s_toLeftByWarp[warpIndex+1]-s_toLeftByWarp[warpIndex]))
				assert(value(t_currElem)<=pivotVal);


			//if(doesGoToLeft){
				//printf("warp: %d, thread: %d, written in shared at pos: %d, value: %6.4e\n", warpIndex, inWarpIndex, toWriteInSharedInd, value(t_currElem));
			//}

			//s_ki[toWriteInSharedInd]=t_currElem;
			copy(s_ki,toWriteInSharedInd,t_currElem);

			

			//coalesced writing to global memory
			unsigned int toWriteInGlobalInd=toWriteGlobIndex(inWarpIndex, warpIndex, s_toLeftByWarp[warpIndex], w_toLeft, w_toRight, beginIndex, endIndex);
			assert(toWriteInGlobalInd<rowLength);

			//if(doesGoToLeft)
			/*if(toWriteInGlobalInd==6)
			{
				printf("it: %d, warp: %d, value: %6.4e %6.4e, in shared: %d, in global: %d, to left: %d, to right: %d, start at: %d, goes to left? %d\n", 
					ch, warpIndex, value(t_currElem), value(s_ki, toWriteInSharedInd), toWriteInSharedInd, toWriteInGlobalInd, w_toLeft, w_toRight, s_toLeftByWarp[warpIndex], doesGoToLeft);
			}*/
			int inSharedInd=warpIndex*32+inWarpIndex;

			//if(toWriteInGlobalInd==6)
				//printf("Trying to catch: %6.4e %6.4e, in shared index: %d\n", value(t_currElem), value(s_ki, inSharedInd), toWriteInSharedInd);
				
			
			//copy(splitArrs, toWriteInGlobalInd, s_ki[inSharedInd]);
			copy(splitArrs, toWriteInGlobalInd, s_ki, inSharedInd);
		}//if(validIndex)
		__syncthreads();

		endIndex-=32*warpsPB-s_toLeftByWarp[warpsPB];
		beginIndex+=s_toLeftByWarp[warpsPB];
			
			
		//}

		elemsLeft-=blockDim.x;
		shift+=blockDim.x;

		movedToLeft+=s_toLeftByWarp[warpsPB];
		
	}

	for(int i=0; i<rowLength; ++i){//TODO: comment or delete!
		//assert(splitRowValues[i]<1E+36);
		//assert(splitRowValues[i]>-1E+36);
		assert(isValidValue(splitArrs,i));
	}

	/*if(inWarpIndex==0)
		printf("Pivot is: %5.3e\n", pivotVal);
	if(index==0){

		//printf("\n");

		printf("**** %d extracted ", movedToLeft);
		for(int i=0; i<movedToLeft; ++i){
			printf("%6.4e ", value(splitArrs,i));
			//assert();
		}
		printf("\n");
	}*/

	assert(movedToLeft>=0);
	assert(movedToLeft<=rowLength);

	return movedToLeft;

}


template <typename T, int warpsPB>
__global__ 
void NthSelectTBKernel(T data, 
	T auxDataArray,
	CLArguments const clArgs,
	Partition<T> *partitions, //maxIterationSupported*numberOfRows elements
	unsigned int *partitionsCount)
{
	int const index=threadIdx.x;//thread's index in a block/row
	int const threadInWarpIndex=threadIdx.x%32;
	int const warpInBlockInd=threadIdx.x>>5;
	int const blockIndex=blockIdx.x;

	assert(warpInBlockInd<clArgs.warpsPB);

	//const unsigned int suspitiousRow=10;
	
	int const rowPad=clArgs.rowLength()*blockIndex;

	__shared__ T s_rowSourceBegin;
	__shared__ T s_rowDestBegin;

	__shared__ int s_chunkLength, s_beginIndex;
	__shared__ bool s_break;

	//extern __shared__ char s_buff[];
	//unsigned int *s_toLeftByWarp=(unsigned int *)s_buff;
	//__shared__ char ss_ki[32*gd_warpsPBConst*(sizeof(float)+sizeof(int))];
	
	//T s_ki;
	//assign(s_ki, ss_ki, 32*clArgs.warpsPB);
	//__syncthreads();

	//const unsigned maxIterationSupported=64;

	//__shared__ Partition<T> s_partitions[maxIterationSupported];
	__shared__ int s_partitionsCount;

	if(index==0){

		s_rowSourceBegin=shifted(data, rowPad);

		//s_rowDestBegin.values=dist2+rowPad;
		//s_rowDestBegin.indices=indecies2+rowPad;
		s_rowDestBegin=shifted(auxDataArray, rowPad);

		s_chunkLength=clArgs.rowLength();
		assert(s_chunkLength<=clArgs.rowLength());
		s_beginIndex=0;
		s_break=false;
		s_partitionsCount=0;
	}
	__syncthreads();

	RNG1 rng(blockIndex);

	int i=0;
	for(; i<min(maxIterationSupported,(int)clArgs.rowLength());++i){
		assert(s_chunkLength>1);
		assert(s_chunkLength<=clArgs.rowLength());
		
		//generating a RN for every row/block
		__shared__ int rvi;
		if(index==0){
			//printf("Threads per row (in a block): %d\n", blockDim.x);
			//RNG1(blockIndex);
			rvi=rng.intInRange(0, s_chunkLength-1, blockIndex);
			assert(rvi>=0);
			assert(rvi<s_chunkLength);
			//printf("rn: %d, s_chunkLength: %d\n", rvi, s_chunkLength);
			//rvi=1;
		}
		__syncthreads();

		//KeyIndexArray source(s_rowSourceBegin.values, s_rowSourceBegin.indices);
		//KeyIndexArray split(s_rowDestDistBegin, s_rowDestIndicesBegin);

	
		//int movedToLeft=splitByPivot(index, blockIndex, s_chunkLength, threadInWarpIndex, warpInBlockInd, s_rowSourceDistBegin[rvi], 
			//s_rowSourceDistBegin, s_rowSourceIndicesBegin, s_rowDestDistBegin, s_rowDestIndicesBegin);
		int movedToLeft=splitByPivot<T, warpsPB>(index, blockIndex, s_chunkLength, threadInWarpIndex, warpInBlockInd, value(s_rowSourceBegin,rvi), 
			s_rowSourceBegin, s_rowDestBegin);
		

		//return;
		/*if(index==0&&blockIdx.x==suspitiousRow){
			printf("\npivot: %d/%5.2e, moved to left: %d\n", rvi, s_rowSourceDistBegin[rvi], movedToLeft);
			for(int j=0; j<s_chunkLength; ++j){
				printf("%5.2e ", s_rowSourceDistBegin[j]);
			}
			printf("\n");

			for(int j=0; j<s_chunkLength; ++j){
				printf("%5.2e ", s_rowDestDistBegin[j]);
			}
			printf("\n");
		}*/

				
				
		if(index==0){

			//printf("moved to left: %d, chunk length: %d, begin index: %d, condition? %d\n", movedToLeft, s_chunkLength, s_beginIndex, clArgs.k-s_beginIndex<movedToLeft);

			if(clArgs.k-s_beginIndex<movedToLeft){//k is in the left side
				s_chunkLength=movedToLeft;
				//if(blockIdx.x==suspitiousRow)
				//printf("k is on the left, moved: %d, chunk ind: %d\n\n", 
					//movedToLeft, s_partitionsCount);
			}else if(clArgs.k-s_beginIndex>movedToLeft){//k is in the right side

				
				s_chunkLength-=movedToLeft;
				
				s_beginIndex+=movedToLeft;

				//Partition p(s_rowDestBegin.values, s_rowDestBegin.indices, movedToLeft);
				//s_partitionsQueue[warpInBlockInd].push_back(p);
				partitions[blockIndex*maxIterationSupported+s_partitionsCount]=Partition<T>(s_rowDestBegin, movedToLeft);
				//if(blockIdx.x==suspitiousRow)
					//printf("k is on the right, moved: %d, chunk ind: %d\n\n", 
						//partitions[s_partitionsCount].length, s_partitionsCount);
				++s_partitionsCount;

			}else{
				
				s_break=true;
				__syncthreads();//all warps need to stop
				//Partition p(s_rowDestBegin.values, s_rowDestBegin.indices, movedToLeft);

				partitions[blockIndex*maxIterationSupported+s_partitionsCount]=Partition<T>(s_rowDestBegin, movedToLeft);
				//if(blockIdx.x==suspitiousRow)
				//	printf("k is found, moved: %d, chunk ind: %d\n\n", 
						//partitions[s_partitionsCount].length, s_partitionsCount);
				++s_partitionsCount;

			}

			//if(index==0){
				//printf("\n");
			//}
		}
		__syncthreads();
		
				

		if(index==0){
			int shift=rowPad+s_beginIndex;//TODO: check swap instead
			if(i%2==1){
				//s_rowSourceBegin.values=dist1+shift;
				//s_rowSourceBegin.indices=indecies1+shift;
				s_rowSourceBegin=shifted(data, shift);

				//s_rowDestBegin.values=dist2+shift;
				//s_rowDestBegin.indices=indecies2+shift;
				s_rowDestBegin=shifted(auxDataArray, shift);
			}else{
				//s_rowSourceBegin.values=dist2+shift;
				//s_rowSourceBegin.indices=indecies2+shift;
				s_rowSourceBegin=shifted(auxDataArray, shift);

				//s_rowDestBegin.values=dist1+shift;
				//s_rowDestBegin.indices=indecies1+shift;
				s_rowDestBegin=shifted(data, shift);
			}
		}
		__syncthreads();

		assert(i!=maxIterationSupported);


	//	if(s_chunkLength[warpInBlockInd]<32)
		//		s_break[warpInBlockInd]=true;
		
		//if(inWarpIndex&&s_chunkLength[warpInBlockInd]<2)
					//s_break[warpInBlockInd]=true;
		
		//printf("???here???\n");
		if(s_break){
			//printf("???here111???\n");
			break;
		}

	}//for i

	/*if(i==maxIterationSupported){
		*error=true;
	}*/

	__shared__ Partition<T> s_partition;

	if(index==0){
		partitionsCount[blockIdx.x]=s_partitionsCount;
		//s_rowSourceBegin.values=dist1+rowPad;
		//s_rowSourceBegin.indices=indecies1+rowPad;
		s_rowSourceBegin=shifted(data, rowPad);
	}

	
	
}


template <typename T, int warpsPB>
__global__ 
void NthCombineKernel(T data, 
	T auxDataArray,
	CLArguments const clArgs,
	Partition<T> *partitions, //maxIterationSupported*numberOfRows elements
	unsigned int *partitionsCount)
{
//	int const index=threadIdx.x;//thread's index in a block/row
//	int const threadInWarpIndex=threadIdx.x%32;
//	int const warpInBlockInd=threadIdx.x>>5;
	int const blockIndex=blockIdx.x;

//	assert(warpInBlockInd<clArgs.warpsPB);

	unsigned int  pc=partitionsCount[blockIndex];

	//const unsigned int suspitiousRow=10;

	//Partition<T> s_partition;

	
	int const rowPad=clArgs.rowLength()*blockIndex;

	T rowSourceBegin=shifted(data, rowPad);


	//printf("%d\n", pc);

	for(int i=0; i<pc; ++i){

		Partition<T> prt=partitions[blockIndex*maxIterationSupported+i];

		//if(threadIdx.x==0){
			//printf("Combining, chunk: %d from %d\n", i, pc);
		//}

		int prtLen=prt.length;

		if(threadIdx.x==0){
			//for(int i=0; i<prtLen; ++i){
				//printf("%6.4e ", value(prt.val, i));
			//}
			//printf("\n");

		}
			
		
		/*if(warpInBlockInd==0){

			warpCopy(threadInWarpIndex, rowSourceBegin, prt.val, prtLen);
		
		}*/

		
		blockCopy(threadIdx.x, rowSourceBegin, prt.val, prtLen);

		shift(rowSourceBegin, prtLen);

	}
}

__global__ 
void PartialQuickSortKernel(float *dist1, int *indecies1, 
	float *dist2, int *indecies2,
	CLArguments const clArgs)
{

	int const index=threadIdx.x+blockIdx.x*blockDim.x;
	int const inWarpIndex=index%warpSize;
	int const rowIndex=index/warpSize;
	if(rowIndex>=clArgs.numberOfRows)
		return;

	int const warpInBlockInd=rowIndex%warpsPerBlock;
	
	int const rowPad=clArgs.rowLength()*rowIndex;

	__shared__ int s_pivotIndex[warpsPerBlock];
	__shared__ float *s_rowSourceDistBegin[warpsPerBlock];
	__shared__ float *s_rowDestDistBegin[warpsPerBlock];
	__shared__ int *s_rowSourceIndicesBegin[warpsPerBlock];
	__shared__ int *s_rowDestIndicesBegin[warpsPerBlock];
	__shared__ int s_chunkLength[warpsPerBlock], s_beginIndex[warpsPerBlock];
	__shared__ bool s_break[warpsPerBlock];

//	__shared__ int s_lengthToWriteBack[warpsPerBlock];
	RNG rng;

	if(inWarpIndex==0){
		s_rowSourceDistBegin[warpInBlockInd]=dist1+rowPad;
		s_rowDestDistBegin[warpInBlockInd]=dist2+rowPad;
		s_rowSourceIndicesBegin[warpInBlockInd]=indecies1+rowPad;
		s_rowDestIndicesBegin[warpInBlockInd]=indecies2+rowPad;
		s_chunkLength[warpInBlockInd]=clArgs.rowLength();
		s_beginIndex[warpInBlockInd]=0;
		s_break[warpInBlockInd]=false;
	}

	//__shared__ Queue<Partition> s_partitionsQueue[warpsPerBlock];
	__shared__ Partition<KeyIndexArray> s_partitions[warpsPerBlock][64];
	__shared__ int s_partitionsCount[warpsPerBlock];

	if(inWarpIndex==0){
		s_partitionsCount[warpInBlockInd]=0;
	}

	__syncthreads();

	//	int chunkLengthToCheck=0;
	for(int i=0;i<clArgs.rowLength();++i){

		int pivotInd=PartitionBallot(rowIndex, inWarpIndex,
				warpInBlockInd,
				s_pivotIndex+warpInBlockInd,
				s_rowSourceDistBegin[warpInBlockInd], s_rowSourceDistBegin[warpInBlockInd]+s_chunkLength[warpInBlockInd], 
				s_rowSourceIndicesBegin[warpInBlockInd],
				s_rowDestDistBegin[warpInBlockInd], s_rowDestIndicesBegin[warpInBlockInd],
				clArgs.k-s_beginIndex[warpInBlockInd],
				rng);

		
		
		if(inWarpIndex==0){

			if(clArgs.k-s_beginIndex[warpInBlockInd]<pivotInd+1){//k is in the left side
				s_chunkLength[warpInBlockInd]=pivotInd+1;

				if(s_chunkLength[warpInBlockInd]==2&&
					*(s_rowSourceDistBegin[warpInBlockInd]+0)==*(s_rowSourceDistBegin[warpInBlockInd]+1))
				{
					//printf("went to left\n");
				}
				//s_lengthToWriteBack[warpInBlockInd]=pivotInd+1;

			//	printf("first cond: starting at: %d; legnth: to wright %d; pivot index: %d\n", s_beginIndex[warpInBlockInd], s_chunkLength[warpInBlockInd], pivotInd);
			}else if(clArgs.k-s_beginIndex[warpInBlockInd]>pivotInd+1){//k is in the right side

				if(s_chunkLength[warpInBlockInd]==2&&
					*(s_rowSourceDistBegin[warpInBlockInd]+0)==*(s_rowSourceDistBegin[warpInBlockInd]+1))
				{
					//printf("went to right\n");
				}
				
				//s_lengthToWriteBack[warpInBlockInd]=s_chunkLength[warpInBlockInd];

			//	printf("second cond: starting at: %d; legnth to wright: %d; pivot index: %d\n", s_beginIndex[warpInBlockInd], s_chunkLength[warpInBlockInd], pivotInd);

				s_chunkLength[warpInBlockInd]-=pivotInd+1;
				s_beginIndex[warpInBlockInd]+=pivotInd+1;

				s_partitions[warpInBlockInd][s_partitionsCount[warpInBlockInd]]=Partition<KeyIndexArray>(KeyIndexArray(s_rowDestDistBegin[warpInBlockInd], s_rowDestIndicesBegin[warpInBlockInd]), pivotInd+1);
				++s_partitionsCount[warpInBlockInd];

			}else{
				s_break[warpInBlockInd]=true;
				s_partitions[warpInBlockInd][s_partitionsCount[warpInBlockInd]]=Partition<KeyIndexArray>(KeyIndexArray(s_rowDestDistBegin[warpInBlockInd], s_rowDestIndicesBegin[warpInBlockInd]), pivotInd+1);
				++s_partitionsCount[warpInBlockInd];

				//printf("third cond\n");
			}

		

		}

		

		//warpCopy(inWarpIndex, s_rowSourceDistBegin[warpInBlockInd], s_rowDestDistBegin[warpInBlockInd], s_lengthToWriteBack[warpInBlockInd]);
		//warpCopy(inWarpIndex, s_rowSourceIndicesBegin[warpInBlockInd], s_rowDestIndicesBegin[warpInBlockInd], s_lengthToWriteBack[warpInBlockInd]);


		

		if(inWarpIndex==0){
			if(i%2==1){
				s_rowSourceDistBegin[warpInBlockInd]=dist1+rowPad+s_beginIndex[warpInBlockInd];
				s_rowDestDistBegin[warpInBlockInd]=dist2+rowPad+s_beginIndex[warpInBlockInd];
				s_rowSourceIndicesBegin[warpInBlockInd]=indecies1+rowPad+s_beginIndex[warpInBlockInd];
				s_rowDestIndicesBegin[warpInBlockInd]=indecies2+rowPad+s_beginIndex[warpInBlockInd];
			}else{
				s_rowSourceDistBegin[warpInBlockInd]=dist2+rowPad+s_beginIndex[warpInBlockInd];
				s_rowDestDistBegin[warpInBlockInd]=dist1+rowPad+s_beginIndex[warpInBlockInd];
				s_rowSourceIndicesBegin[warpInBlockInd]=indecies2+rowPad+s_beginIndex[warpInBlockInd];
				s_rowDestIndicesBegin[warpInBlockInd]=indecies1+rowPad+s_beginIndex[warpInBlockInd];
			}
		}


	//	if(s_chunkLength[warpInBlockInd]<32)
		//		s_break[warpInBlockInd]=true;
		
		if(inWarpIndex&&s_chunkLength[warpInBlockInd]<2)
					s_break[warpInBlockInd]=true;
		
	
		if(s_break[warpInBlockInd])
			break;

	} 

	__shared__ Partition<KeyIndexArray> s_partition[warpsPerBlock];

	if(inWarpIndex==0){
		s_rowSourceDistBegin[warpInBlockInd]=dist1+rowPad;
		s_rowSourceIndicesBegin[warpInBlockInd]=indecies1+rowPad;
	}

	//while(!s_partitionsQueue[warpInBlockInd].is_empty()){
	for(int i=0; i<s_partitionsCount[warpInBlockInd]; ++i){
	
		if(inWarpIndex==0){
			//s_partition[warpInBlockInd]=s_partitionsQueue[warpInBlockInd].pop_front();
			s_partition[warpInBlockInd]=s_partitions[warpInBlockInd][i];
		}

	

		warpCopy(inWarpIndex, s_rowSourceDistBegin[warpInBlockInd], s_partition[warpInBlockInd].val.values, s_partition[warpInBlockInd].length);
		warpCopy(inWarpIndex, s_rowSourceIndicesBegin[warpInBlockInd], s_partition[warpInBlockInd].val.indices, s_partition[warpInBlockInd].length);

		if(inWarpIndex==0){
			s_rowSourceDistBegin[warpInBlockInd]+=s_partition[warpInBlockInd].length;
			s_rowSourceIndicesBegin[warpInBlockInd]+=s_partition[warpInBlockInd].length;
		}
	}

}

void PartialQuickSort(float *dist, int *indecies, float *distAux, int *indeciesAux, CLArguments const &clArgs){
	
	int numberOfBlocks=DivUp(clArgs.numberOfRows, warpsPerBlock);

	cout<<"numberOfBlocks="<<numberOfBlocks<<endl;
	cout<<"numberOfWarps="<<numberOfBlocks*warpsPerBlock<<endl;
	PartialQuickSortKernel<<<numberOfBlocks, warpsPerBlock*warpSize>>>(dist, indecies, distAux, indeciesAux, clArgs);
	assert(cudaGetLastError()==cudaSuccess);
	
} 

template <typename T>
void NthSelectTB(T data, T dataAux, CLArguments const &clArgs,
	Partition<T> *partitions, //maxIterationSupported*numberOfRows elements
	unsigned int *partitionsCount){
	
	int numberOfBlocks=clArgs.numberOfRows;//DivUp(, warpsPerBlock);

	//cout<<"numberOfBlocks="<<numberOfBlocks<<endl;
	//cout<<"numberOfWarps="<<numberOfBlocks*clArgs.warpsPB<<endl;
	//cout<<"blockSize="<< clArgs.warpsPB*warpSize<<endl;

	//int warpPB=12;
	//cudaMemcpyToSymbol(gd_warpsPB, &clArgs.warpsPB, sizeof(int));

#define call(wpb)\
	NthSelectTBKernel<T, wpb><<<numberOfBlocks, wpb*warpSize, ILP_LEVEL*(wpb+1)*sizeof(int)+warpSize*wpb*sizeof(typename Traits<T>::Elem_t)>>>(data, dataAux, clArgs, partitions, partitionsCount); \
	NthCombineKernel<T, wpb><<<numberOfBlocks, wpb*warpSize>>>(data, dataAux, clArgs, partitions, partitionsCount)


	//calls' dispatcher. I was unable to make working version of a fast kernel where I can pass number of warps in a block as a parameter
	switch(clArgs.warpsPB){
	case 1:
		call(1); break;
	case 2:
		call(2); break;
	case 3:
		call(3); break;
	case 4:
		call(4); break;
	case 5:
		call(5); break;
	case 6:
		call(6); break;
	case 7:
		call(7); break;
	case 8:
		call(8); break;
	case 9:
		call(9); break;
	case 10:
		call(10); break;
	case 11:
		call(11); break;
	case 12:
		call(12); break;
	case 13:
		call(13); break;
	case 14:
		call(14); break;
	case 15:
		call(15); break;
	case 16:
		call(16); break;
	case 17:
		call(17); break;
	case 18:
		call(18); break;
	case 19:
		call(19); break;
	case 20:
		call(20); break;
	case 21:
		call(21); break;
	case 22:
		call(22); break;
	case 23:
		call(23); break;
	case 24:
		call(24); break;
	case 25:
		call(25); break;
	case 26:
		call(26); break;
	case 27:
		call(27); break;
	case 28:
		call(28); break;
	case 29:
		call(29); break;
	case 30:
		call(30); break;
	case 31:
		call(31); break;
	case 32:
		call(32); break;

	default:
		cerr<<"Wrong number of warps in a block requested\n";
	}

	
#undef call
	cudaError_t res=cudaGetLastError();

	if(res!=cudaSuccess){
		throw std::runtime_error(cudaGetErrorString(res));
	}
	
	//assert(cudaGetLastError()==cudaSuccess);
	
}

template void NthSelectTB<KeyIndexArray>(KeyIndexArray data, KeyIndexArray dataAux, CLArguments const &clArgs, Partition<KeyIndexArray> *partitions, unsigned int *partitionsCount);
template void NthSelectTB<float *>(float *data, float *dataAux, CLArguments const &clArgs, Partition<float *> *partitions, unsigned int *partitionsCount);

void ExtractIndices(float const *allDist, float const *extractedIndices, int *indices, CLArguments const &clArgs){
	
	int numberOfBlocks=DivUp(clArgs.numberOfRows, warpsPerBlock);

	cout<<"numberOfBlocks="<<numberOfBlocks<<endl;
	cout<<"numberOfWarps="<<numberOfBlocks*warpsPerBlock<<endl;
	ExtractIndicesKernel<<<numberOfBlocks, warpsPerBlock*warpSize>>>(allDist, extractedIndices, indices, clArgs);
	assert(cudaGetLastError()==cudaSuccess);
	
} 





