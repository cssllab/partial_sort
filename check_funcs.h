#ifndef CHECK_FUNC_H
#define CHECK_FUNC_H

#include <thrust/device_vector.h>

struct CLArguments;

void CheckSolution(float *dist, CLArguments const &clArgs);

void CheckSolution(thrust::device_vector<float> &dist, thrust::device_vector<int> &indices, 
	thrust::device_vector<float> const &distSelected,
	thrust::device_vector<int> const &columnIndicesSelected,
	CLArguments const &clArgs);

#endif