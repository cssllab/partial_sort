#ifndef KERNELS_H
#define KERNELS_H

#include "CLArguments.h"
#include <iostream>

extern const int warpSize;
#define warpsPerBlock 12
#define ILP_LEVEL 4
#define maxIterationSupported 64//a kernel can't have more than maxIterationSupported internal iteraions. Not a really nice limitation...

__forceinline__
__host__ __device__ 
int DivUp(int value, int div){
	/*if(value%div==0)
		return value/div;
	else
		return value/div+1;*/
	return (value+div-1)/div;
}

template <typename T>
struct Partition{
	T val;

	int length;

	__device__ __inline__
	Partition(){}

	__device__ __inline__
	Partition(T v, int l):val(v), length(l){}
};


void PartialQuickSort(float *dist, int *indecies, float *distAux, int *indeciesAux, CLArguments const &clArgs);
void ExtractIndices(float const *allDist, float const *extractedIndices, int *indices, CLArguments const &clArgs);

template <typename T>
void NthSelectTB(T data, T dataAux, CLArguments const &clArgs,
	Partition<T> *partitions, //maxIterationSupported*numberOfRows elements
	unsigned int *partitionsCount);

#endif