#ifndef COLORTEXT_H
#define  COLORTEXT_H

#ifdef WIN32
#include <windows.h>
class ColorText{ 
public: 
    ColorText(WORD _wAttributes) : wAttributes(_wAttributes) {}; 

    friend std::ostream& operator<<(std::ostream& output, const ColorText &dt) 
    { 
        // Use: 
        //   STD_OUTPUT_HANDLE with 'cout' 
        //   STD_ERROR_HANDLE  with 'cerr' 
        SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), dt.wAttributes); 
        return output; 
    }; 

	~ColorText(){
		 SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_BLUE|FOREGROUND_GREEN|FOREGROUND_RED);
	}

private: 
    WORD wAttributes; 
}; 
#else
#ifndef FOREGROUND_BLUE
#define FOREGROUND_BLUE  0x0001
#define FOREGROUND_GREEN 0x0002
#define FOREGROUND_RED  0x0004
#define FOREGROUND_INTENSITY 0x0008
#endif
class ColorText{
	 unsigned int wAttributes; 
public: 
	ColorText(int _wAttributes) : wAttributes(_wAttributes) {}; 
	friend std::ostream& operator<<(std::ostream& output, const ColorText &dt) {return output;}
};
#endif

#endif
