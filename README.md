# README #

This is a GPU- accelerated algorithm that rearranges the elements in multiple arrays in such a way that the first smallest k elements in every array become located at positions [0, k). The remaining elements are not preserved. The algorithm also extract original indices for selected elements. 


### How do I get set up? ###

Use [ CMake ](http://www.cmake.org/download/) to produce a project file for your compiler of choice. You will need [NVIDIA CUDA (tm) SDK](https://developer.nvidia.com/cuda-downloads) in order to build the project, CUDA 4.2 was used for development. This code supports only NVIDIA video cards.

Available command-line arguments are:

* ``-r <number>`` to choose number of arrays to process
* `-c <number>` to set the length of arrays
* `-k <number>` to set the number of elements-in-question to be selected
* `-d <number>` to set a GPU index in case you have several of them installed
* `-v` to extract values only, without indices

For example, `PartialSort.exe -r 1024 -c 2048 -k 100` will be working on 1024 arrays each consisting of 2048 elements and exracting 100 smallest elements from every array along with their original indices.

### What does it do? ###
The algorithm starts with initializing "dist" float array (see main() function) with r*c elements and populates it with psedu-random numbers. This array is then sent over to the GPU side along with auxilary information.
NthSelectTB algorithm is called to perform the partitioning, results are written back into "dist" array. Original indices are stored and get rearranged within "columnIndices" array.
The entire process repeats 100 times (hardcoded) to obtain average computing time. Statistics is written into my_stat.txt file in the current directory.

### Disclaimer ###
No particular effort were taken to make this code ready-to-use, you may need to modify the source code to get it working for your case. Not all combinations of parameters have been tested.

###The MIT License (MIT)###
The MIT License (MIT)

Copyright (c) <2010> <Dr. Roshan D'Souza, Ivan Komarov, Ali Dashti, University of Wisconsin-Milwaukee>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.