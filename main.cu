#include <iostream>
#include <stdio.h>
#include "MyTime.h"
#include "kernels.h"
#include "CLArguments.h"
#include "thrust_based.h"
#include "check_funcs.h"
#include "ColorText.h"
#include <algorithm>
#include <fstream>
#include <numeric>

#pragma warning (disable: 4244)

#include <thrust/device_vector.h>
#include <thrust/generate.h>
#include <thrust/random.h>
#include <thrust/logical.h>
#include "DataTypes.h"


using std::cout;
using std::endl;
using std::cerr;
using thrust::device_vector;
using thrust::host_vector;
using thrust::raw_pointer_cast;
using thrust::random::default_random_engine;
using thrust::counting_iterator;
using thrust::make_zip_iterator;
using thrust::make_tuple;

const int warpSize=32;

void ParceCLArguments(int argc, char *argv[], CLArguments &clArgs){
	if(argc<7){
		cerr<<"Not enough arguments"<<endl;
		exit(EXIT_FAILURE);
	}
	clArgs.devInd=0;
	clArgs.extraComparison=false;
	clArgs.valuesOnly=false;
	clArgs.warpsPB=12;
	clArgs.fStatName[0]=0;

	for(int arg=1; arg<argc; ++arg){
		if(strcmp(argv[arg], "-r")==0){
			++arg;
			clArgs.numberOfRows=atoi(argv[arg]);
			continue;
		}
		if(strcmp(argv[arg], "-c")==0){
			++arg;
			clArgs.numberOfColumns=atoi(argv[arg]);
			continue;
		}
		if(strcmp(argv[arg], "-k")==0){
			++arg;
			clArgs.k=atoi(argv[arg]);
			continue;
		}
		if(strcmp(argv[arg], "-d")==0){
			++arg;
			clArgs.devInd=atoi(argv[arg]);
			continue;
		}

		if(strcmp(argv[arg], "-e")==0){
			clArgs.extraComparison=true;
			continue;
		}

		if(strcmp(argv[arg], "-v")==0){
			clArgs.valuesOnly=true;
			continue;
		}

		if(strcmp(argv[arg], "-w")==0){
			++arg;
			clArgs.warpsPB=atoi(argv[arg]);
			continue;
		}

		if(strcmp(argv[arg], "-o")==0){
			++arg;
			strcpy(clArgs.fStatName, argv[arg]);
			
			continue;
		}
	}

}

void PrintCLArguments(CLArguments const &clArgs, std::ostream &os){
	os<<"Running the algorithm with "<<"\n\t"<<clArgs.numberOfRows<<" rows"<<
		"\n\t"<<clArgs.rowLength()<<" row length"<<
		"\n\t"<<clArgs.numberOfColumns<<" columns"<<
		"\n\t"<<clArgs.k<<" elements are to be extracted"<<
		(clArgs.extraComparison?"\n\textra comparison requested":"")<<
		"\n\t"<<clArgs.devInd<<" GPU device # to be used"<<endl<<
	    "\n\t"<<clArgs.warpsPB<<" warps per block used"<<endl;

	cudaDeviceProp cdp;
	if(cudaGetDeviceProperties(&cdp, clArgs.devInd)==cudaSuccess){
		os<<"GPU device name: "<<cdp.name<<endl;
		os<<"\ttotal memory: "<<cdp.totalGlobalMem/(1024.*1024.)<<"MB"<<endl;
		size_t free, total;
		if(cudaMemGetInfo (&free,&total)==cudaSuccess)
			os<<"\t"<<free/(1024.*1024.)<<"MB available"<<endl;
		os<<"\tnumber of multiprocessors: "<<cdp.multiProcessorCount<<endl;
		
	}
	os<<clArgs.totalSize()*(sizeof(float)+sizeof(int))/(1024.*1024.)*2<<" MB of global memory to be used for GPU nth_algorithm\n";
	os<<endl;

}


struct RandomNumberGen
{ 
	CLArguments m_clArgs;

	RandomNumberGen(CLArguments clArgs):m_clArgs(clArgs){}

	__device__ 
	float operator()(int index)const 
	{ 
		int columnIndex=index%m_clArgs.rowLength();
		if(columnIndex>=m_clArgs.rowLength())
			return 1E+30f;
		else{
			default_random_engine rng(clock()); //remove parameter if you need repetitive results
			rng.discard(index+1); // Skip past numbers used in previous threads. 
			return rng();
		}
	} 
};

struct RowIndicesGen{
	int m_rowLength;
	RowIndicesGen(int rowLength):m_rowLength(rowLength){}

	__device__
	int operator()(int i)const{
		return i/m_rowLength;
	}
};

struct ColumnIndicesGen{
	int m_rowLength;
	ColumnIndicesGen(int rowLength):m_rowLength(rowLength){}

	__device__
	int operator()(int i)const{
		return i%m_rowLength;
	}
};

void ExtractSelected(device_vector<float> &distSelected,
		device_vector<int> &columnIndicesSelected, 
		device_vector<float> const &dist,
		device_vector<int> const &indices,
		CLArguments clArgs)
{
	distSelected.resize(clArgs.numberOfRows*clArgs.k);
	columnIndicesSelected.resize(clArgs.numberOfRows*clArgs.k);
	cout<<"Extracting selected elements... ";
	for(int i=0; i<clArgs.numberOfRows; ++i){
		int rowShift=clArgs.rowLength()*i;
		thrust::copy(indices.begin()+rowShift, indices.begin()+rowShift+clArgs.k, columnIndicesSelected.begin()+clArgs.k*i);
		thrust::copy(dist.begin()+rowShift, dist.begin()+rowShift+clArgs.k, distSelected.begin()+clArgs.k*i);
	}
	cout<<"done\n";
}


void PrintAtMost(int maxEl, CLArguments clArgs, device_vector<int> const &dist, device_vector<int> const &distSelected)
{
	host_vector<int> h_distToCheck(clArgs.k);
	thrust::copy(dist.begin(), dist.begin()+clArgs.k, h_distToCheck.begin());
	//thrust::sort(h_distToCheck.begin(), h_distToCheck.end());
	//cudaThreadSynchronize();//We can potentially have wrong timing here
	//cout<<"Equal? "<<thrust::equal(dist.begin(), dist.begin()+clArgs.k, distSelected.begin())<<endl;
	for(int j=0; j<min(maxEl, clArgs.k); ++j){
		int v1=dist[j];
		int v2=distSelected[j];
		printf("%d %d %d\n", j, v1, v2);
	}
}


void PrintAtMost(int maxEl, CLArguments clArgs, device_vector<float> const &dist, device_vector<float> const &distSelected)
{
	host_vector<float> h_distToCheck(clArgs.k);
	thrust::copy(dist.begin(), dist.begin()+clArgs.k, h_distToCheck.begin());
	//thrust::sort(h_distToCheck.begin(), h_distToCheck.end());
	//cudaThreadSynchronize();//We can potentially have wrong timing here
	//cout<<"Equal? "<<thrust::equal(dist.begin(), dist.begin()+clArgs.k, distSelected.begin())<<endl;
	for(int j=0; j<min(maxEl, clArgs.k); ++j){
		float v1=dist[j];
		float v2=distSelected[j];
		printf("%d %e %e\n", j, v1, v2);
	}
}
		

std::ofstream g_statFile;


int main(int argc, char *argv[]){

	try{

		

		CLArguments clArgs;

		ParceCLArguments(argc, argv, clArgs);

		if(strlen(clArgs.fStatName)>1){
			g_statFile.open(clArgs.fStatName, std::ios::app);
		}else{
			g_statFile.open("my_stat.txt", std::ios::app);
		}

		if(!g_statFile.is_open()){
			cerr<<"Can't open statistic file\n";
			return (EXIT_FAILURE);
		}

		PrintCLArguments(clArgs, cout);
		PrintCLArguments(clArgs, g_statFile);

		if(clArgs.k>clArgs.numberOfColumns){
			cerr<<"Number of elements to select from every row can not exceed number of columns"<<endl;
			exit(EXIT_FAILURE);
		}

		cudaSetDevice(clArgs.devInd);

		cout<<"Initializing arrays... ";
		device_vector<float> dist(clArgs.totalSize());
		device_vector<int> rowIndices;
		device_vector<int> columnIndices;
		if(!clArgs.valuesOnly){
			columnIndices.resize(clArgs.totalSize());
		}

		cout<<"done."<<endl;
	
		cout<<"Initializing auxiliary arrays...";
		device_vector<float> distAux(clArgs.totalSize());
		device_vector<int> columnIndicesAux;
		if(!clArgs.valuesOnly){
			columnIndicesAux.resize(clArgs.totalSize());
		}
		cout<<"done"<<endl;

		cout<<"Initializing distances with random numbers... ";
		thrust::counting_iterator<int> cit(0);
		thrust::transform(cit, cit+clArgs.totalSize(), dist.begin(), RandomNumberGen(clArgs));
		cout<<"done."<<endl;
	
		if(!clArgs.valuesOnly){
			cout<<"Initializing column indices... ";
			thrust::transform(cit, cit+clArgs.totalSize(), columnIndices.begin(), ColumnIndicesGen(clArgs.rowLength()));
			cout<<"done."<<endl;
		}


		device_vector<float> distSelected;
		device_vector<int> columnIndicesSelected;
		host_vector<float> h_dist;
		host_vector<int> h_columnIndices;
		MyTime::Time_t tb, te;
		if(clArgs.extraComparison){
			if(clArgs.valuesOnly){
				cerr<<"Checking and values-only flags are incompatable (so far)\n";
				return EXIT_FAILURE;
			}

			cout<<"Saving arrays on the CPU side... ";
			h_dist=dist;
			host_vector<int> h_columnIndices=columnIndices;
			cout<<"done."<<endl;

			
			cout<<"Initializing row indices... ";
			rowIndices.resize(clArgs.totalSize());
			thrust::transform(cit, cit+clArgs.totalSize(), rowIndices.begin(), RowIndicesGen(clArgs.rowLength()));
			cout<<"done."<<endl;

			cout<<"Sorting with thrust... ";
			tb=MyTime::CTime();
			ThrustSort(raw_pointer_cast(&dist[0]), raw_pointer_cast(&rowIndices[0]), raw_pointer_cast(&columnIndices[0]), clArgs);
			cudaThreadSynchronize();//We can potentially have wrong timing here
			te=MyTime::CTime();
			cout<<"done"<<endl;
			CheckIfSorted(raw_pointer_cast(&dist[0]), clArgs, true, raw_pointer_cast(&rowIndices[0]));
		//	device_vector<int>().swap(rowIndices);
			cout<<"It took "<<MyTime::ElapsedTime(tb, te)<<" ms to complete the reference thrust algorithm"<<endl;

			ExtractSelected(distSelected, columnIndicesSelected, dist, columnIndices, clArgs);

		//	ThrustSort(raw_pointer_cast(&dist[0]), raw_pointer_cast(&rowIndices[0]), raw_pointer_cast(&columnIndices[0]), clArgs);

			/*cout<<"Copying reference arrays back to GPU... ";
			thrust::copy(h_dist.begin(), h_dist.end(), dist.begin());
			thrust::copy(h_columnIndices.begin(), h_columnIndices.end(), columnIndices.begin());
			cout<<"done\n";

			cout<<"Sorting with thrust with user-defined comparison (distances only, no column indices)... ";
			tb=MyTime::CTime();
			ThrustSortComp(raw_pointer_cast(&dist[0]), raw_pointer_cast(&rowIndices[0]), raw_pointer_cast(&columnIndices[0]), clArgs);
			cudaThreadSynchronize();//We can potentially have wrong timing here
			te=MyTime::CTime();
			cout<<"done"<<endl;
			CheckIfSorted(raw_pointer_cast(&dist[0]), clArgs, true, raw_pointer_cast(&rowIndices[0]));
			device_vector<int>().swap(rowIndices);
			cout<<"It took "<<MyTime::ElapsedTime(tb, te)<<" ms to complete the reference thrust algorithm"<<endl;*/

		//	for(int i=0; i<1; ++i){
			cout<<"Copying reference arrays back to GPU... ";
			thrust::copy(h_dist.begin(), h_dist.end(), dist.begin());
			thrust::copy(h_columnIndices.begin(), h_columnIndices.end(), columnIndices.begin());
			cout<<"done\n";

			cout<<"Extras from thrust selection:\n";
			PrintAtMost(std::min(10, clArgs.k), clArgs, dist, distSelected);
			PrintAtMost(std::min(10, clArgs.k), clArgs, columnIndices, columnIndicesSelected);

			cout<<"Saving reference arrays on the CPU side... ";
			h_dist=dist;
			h_columnIndices=columnIndices;
			cout<<"done."<<endl;

			//cout<<ColorText(FOREGROUND_GREEN|FOREGROUND_RED)<<" value at position: "<<h_dist[clArgs.rowLength()*58+17827]<<" "<<dist[clArgs.rowLength()*58+17828]<<" "<<dist[clArgs.rowLength()*58+17829]<<endl;
		}//clArgs.k smallest elements are copied into "distSelected" and "columnIndicesSelected" arrays



		
		
		cudaEvent_t start, stop; cudaEventCreate(&start); cudaEventCreate(&stop);

		
		g_statFile<<"min avg max stddev\n";
		const int runs=100;
		std::vector<float> times(runs);
		cout<<"Starting our algorithm... ";
		cout<<"\n";
		for(int i=0; i<runs; ++i){//this loop won't work with checking (-e CL parameter)

			//tb=MyTime::CTime();

			//cout<<"Initializing distances with random numbers... ";
			thrust::counting_iterator<int> cit(0);
			thrust::transform(cit, cit+clArgs.totalSize(), dist.begin(), RandomNumberGen(clArgs));
			//cout<<"done."<<endl;
	
			if(!clArgs.valuesOnly){
				//cout<<"Initializing column indices... ";
				thrust::transform(cit, cit+clArgs.totalSize(), columnIndices.begin(), ColumnIndicesGen(clArgs.rowLength()));
				//cout<<"done."<<endl;
			}
			
			cudaEventRecord(start, 0);
			/*PartialQuickSort(raw_pointer_cast(&dist[0]), raw_pointer_cast(&columnIndices[0]), 
				raw_pointer_cast(&distAux[0]), raw_pointer_cast(&columnIndicesAux[0]), clArgs);*/

			if(clArgs.valuesOnly){
				typedef Partition<float *> Partition_t;
				device_vector<Partition_t> partitions(clArgs.numberOfRows*maxIterationSupported);
				device_vector<unsigned int> partitionsCount(clArgs.numberOfRows);
				NthSelectTB(raw_pointer_cast(&dist[0]), raw_pointer_cast(&distAux[0]), clArgs, raw_pointer_cast(&partitions[0]), raw_pointer_cast(&partitionsCount[0]));
			}else{
				typedef Partition<KeyIndexArray> Partition_t;
				device_vector<Partition_t> partitions(clArgs.numberOfRows*maxIterationSupported);
				device_vector<unsigned int> partitionsCount(clArgs.numberOfRows);
				NthSelectTB(KeyIndexArray(raw_pointer_cast(&dist[0]), raw_pointer_cast(&columnIndices[0])), 
					KeyIndexArray(raw_pointer_cast(&distAux[0]), raw_pointer_cast(&columnIndicesAux[0])), clArgs, raw_pointer_cast(&partitions[0]), raw_pointer_cast(&partitionsCount[0]));
			}

			cudaEventRecord(stop, 0); 
			cudaEventSynchronize(stop); 
			float elapsedTime; 
			cudaEventElapsedTime(&elapsedTime, start, stop);
			//cudaThreadSynchronize();//We can potentially have wrong timing here
			//te=MyTime::CTime();
			cout<<ColorText(FOREGROUND_GREEN|FOREGROUND_INTENSITY)<<i<<"; it took "<<elapsedTime<<" ms to complete the custom algorithm"<<endl;

			times[i]=elapsedTime;
		}

		float mn=*std::min_element(times.begin(), times.end());
		float mx=*std::max_element(times.begin(), times.end());
		

		double sum = std::accumulate(times.begin(), times.end(), 0.0);
		double mean = sum / times.size();

		double sq_sum = std::inner_product(times.begin(), times.end(), times.begin(), 0.0);
		double stdev = std::sqrt(sq_sum / times.size() - mean * mean);


		g_statFile<<mn<<" "<<mean<<" "<<mx<<" "<<stdev<<endl<<endl;
		
		//
		//now clArgs.k extracted elements are placed first into "dist" and "columnIndices" arrays

		
		
		thrust::device_vector<float> d_ks;
		if(clArgs.extraComparison){
			cout<<"Extracting found elements... ";
			//thrust::host_vector<float> h_ks(clArgs.k*clArgs.numberOfRows);
			d_ks.resize(dist.size());
			//thrust::copy(dist.begin(), dist.begin()+clArgs.k*clArgs.numberOfRows, h_ks.begin());
			thrust::copy(dist.begin(),dist.end(), d_ks.begin());
			cout<<"done\n";

			cout<<"Copying reference arrays back to GPU... ";//TODO: what is that???
			thrust::copy(h_dist.begin(), h_dist.end(), dist.begin());
			//thrust::copy(h_columnIndices.begin(), h_columnIndices.end(), columnIndices.begin());
			cout<<"done\n";
		}

		
		//cout<<"Extras from our algorithm selection:\n";
		//PrintAtMost(10, clArgs, dist, d_ks);
		
		/*{
		cout<<"Starting extracting algorithm... ";
		cudaEventRecord(start, 0);
		ExtractIndices(raw_pointer_cast(&dist[0]), raw_pointer_cast(&d_ks[0]), raw_pointer_cast(&columnIndicesAux[0]), clArgs);

		cudaEventRecord(stop, 0); 
		cudaEventSynchronize(stop); 
		elapsedTime; 
		cudaEventElapsedTime(&elapsedTime, start, stop);
		//cudaThreadSynchronize();//We can potentially have wrong timing here
		//te=MyTime::CTime();
		cout<<ColorText(FOREGROUND_GREEN|FOREGROUND_INTENSITY)<<"It took "<<elapsedTime<<" ms to complete the extraction algorithm"<<endl;
		cout<<"done\n";
		}*/

		//PrintAtMost(10, clArgs, columnIndicesAux, columnIndices);
		
		cudaEventDestroy(start); cudaEventDestroy(stop);

	//	cout<<ColorText(FOREGROUND_GREEN|FOREGROUND_RED)<<"max: "<<(float)(*thrust::max_element(dist.begin()+58*clArgs.rowLength(), dist.begin()+58*clArgs.rowLength()+clArgs.k))<<
		//	" value at position: "<<dist[17827]<<" "<<dist[17828]<<" "<<dist[17829]<<endl;
	//	}
		

		//extracted elements are in d_ks
		if(clArgs.extraComparison){
			//reuslts of our algorithm are stored into d_ks and columnIndices arrays

			try{
				//CheckSolution(raw_pointer_cast(&dist[0]), clArgs);
				CheckSolution(d_ks, columnIndices, distSelected, columnIndicesSelected, clArgs);
			}catch(...){
				cerr<<"\nError on checking detected"<<endl;
				throw;
			}
			cudaThreadSynchronize();//We can potentially have wrong timing here

			PrintAtMost(15, clArgs, dist, distSelected);

			cout<<"Starting nth algorithm... ";
			tb=MyTime::CTime();
			for(int i=0; i<clArgs.numberOfRows; ++i){
				float *row=&h_dist[0]+clArgs.rowLength()*i;
				std::nth_element(row, row+clArgs.k, row+clArgs.rowLength());
			}
			te=MyTime::CTime();
			cout<<"done"<<endl;
			cout<<"It took "<<MyTime::ElapsedTime(tb, te)<<" ms to complete the nth std algorithm"<<endl;
		}

	}
	catch(std::exception &ec){
		cerr<<endl<<ColorText(FOREGROUND_RED|FOREGROUND_INTENSITY)<<"Std error: "<<ec.what()<<endl;
		return EXIT_FAILURE;
	}
	/*catch(thrust::exception &ec){
		cerr<<"Unexpected std error: "<<ec.what()<<endl;
		return EXIT_FAILURE;
	}*/
	catch(...){
		cerr<<endl<<ColorText(FOREGROUND_RED|FOREGROUND_INTENSITY)<<"Unexpected error"<<endl;
		return EXIT_FAILURE;
	}

	
	cudaError_t cErr=cudaGetLastError();
	if(cErr==cudaSuccess){
		cout<<"\nFinished. No GPU errors detected\n";
		return EXIT_SUCCESS;
	}else{
		cout<<"\nFinished. GPU error is presenting: "<<cudaGetErrorString(cErr);
		return EXIT_FAILURE;
	}
}

